<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

  protected $headerData = array();
  protected $data = array();

  function __construct() {
    parent::__construct();
    $this->load->model('company_model');
  }

  function render_page($view) {
    //do this to don't repeat in all controllers...
    $this->headerData['is_admin'] = $this->ion_auth->is_admin();
    $this->headerData['user_data'] = $this->ion_auth->user()->row();
    $company_data = $this->company_model->get_company();
    if($company_data != null && count($company_data) > 0){
      $this->headerData['company_data'] = array("CompanyName"=>$company_data[0]->CompanyName);
    }else
    $this->headerData['company_data'] = array("CompanyName"=>"Your Company Name");
    // var_dump($this->headerData,$this->data);
    // exit;
    $this->load->view('common/header', $this->headerData);
    $this->load->view($view, $this->data);
    $this->load->view('common/footer');
  }

}
