<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Sales Order Entry
*/
class Sales_entry_model extends CI_Model
{


	public function __construct()
	{
		$this->tableName = 'sales_order_entry';
		parent::__construct();
		$this->load->database();
	}

	public function get_all_sales()
	{
		$this->db->from($this->tableName);
		$this->db->where("status",0);
		$query=$this->db->get();
		return $query->result();
	}

	public function get_sale_details($SaleId)
	{
		$query = $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("SaleId",$SaleId);
		$result = $query->get()->result_array();
		if(isset($result[0])){
			return $result[0];
		}
		return null;
	}

public function find_stock_entry( $CustomerId , $StockId , $Quantity)
{
		$query = $this->db->select("DISTINCT(a.SalesOrderId),a.*");
		$this->db->from($this->tableName . " as a");
		$this->db->join("sales_order as b" , "b.CustomerId = ".$CustomerId , 'right outer');
		$this->db->where("a.StockId",$StockId);
		$this->db->order_by("a.SalesOrderId", "desc");

		// $this->db->where("a.Quantity",">=", $Quantity);
		return $query->get()->result_array();
}
	public function save_sales_order_entry($data)
	{
		$this->db->insert($this->tableName, $data);
		return $this->db->insert_id();
	}

}
?>
