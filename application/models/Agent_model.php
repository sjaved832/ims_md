<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Agent_model extends CI_Model
{


	public function __construct()
	{
		$this->tableName = 'agent';
		parent::__construct();
		$this->load->database();
	}

	public function get_all_agents()
	{
		$this->db->from($this->tableName);
		$this->db->where("status",0);
		$query=$this->db->get();
		return $query->result();
	}

	public function get_agent_details($AgentId)
	{
		$query = $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("AgentId",$AgentId);
		$result = $query->get()->result_array();
		if(isset($result[0])){
			return $result[0];
		}
		return null;
	}

	public function update_agent($AgentData)
	{
		$query = $this->db->where('AgentId',$AgentData['AgentId']);
		$this->db->update($this->tableName , $AgentData);

		return $query->affected_rows();
	}

	public function save_agent($data)
	{
		$this->db->insert($this->tableName, $data);
		return $this->db->insert_id();
	}
}
?>
