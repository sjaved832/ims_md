<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Deliverychallan extends CI_Model
{

	public function __construct()
	{
		$this->tableName = 'deliverychallan';
		parent::__construct();
		$this->load->database();
	}

	public function get_all_deliverychallans()
	{
		$this->db->from($this->tableName ." as a");
		$this->db->where("a.status",0);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_deliverychallan_details($DeliverychallanId)
	{
		$query = $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("DeliverychallanId",$DeliverychallanId);
		$result = $query->get()->result_array();
		if(isset($result[0])){
			return $result[0];
		}
		return null;
	}
	public function update_deliverychallan($DeliverychallanData)
	{
		$query = $this->db->where('DeliverychallanId',$DeliverychallanData['DeliverychallanId']);
		$this->db->update($this->tableName , $SaleData);

		return $query->affected_rows();
	}

	public function save_deliverychallan($data)
	{
		$this->db->insert($this->tableName, $data);
		return $this->db->insert_id();
	}



}
?>
