<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Transport_model extends CI_Model
{

	public function __construct()
	{
		$this->tableName = 'transport';
		parent::__construct();
		$this->load->database();
	}

	public function get_all_transports()
	{
		$this->db->from($this->tableName);
		$this->db->where("status",0);
		$query=$this->db->get();
		return $query->result();
	}

	public function get_transport_details($TransportId)
	{
		$query = $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("transportId",$TransportId);
		$result = $query->get()->result_array();
		if(isset($result[0])){
			return $result[0];
		}
		return null;
	}

	public function update_transport($TransportData)
	{
		$query = $this->db->where('transportId',$TransportData['transportId']);
		$this->db->update($this->tableName , $TransportData);

		return $query->affected_rows();
	}

	public function save_transport($data)
	{
		$this->db->insert($this->tableName, $data);
		return $this->db->insert_id();
	}

}
