<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Product_color_model extends CI_Model
{

	public function __construct()
	{
		$this->tableName = 'product_color';
		parent::__construct();
		$this->load->database();
	}

	public function get_all_product_colors()
	{
		$this->db->from($this->tableName);
		$this->db->where("status",0);
		$query=$this->db->get();
		return $query->result();
	}

	public function get_product_color_details($ProductColorId)
	{
		$query = $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("ProductColorId",$ProductColorId);
		$result = $query->get()->result_array();
		if(isset($result[0])){
			return $result[0];
		}
		return null;
	}

	public function get_product_colors($ProductId)
	{
		$query = $this->db->select("a.* ,b.ColorName");
		$this->db->from($this->tableName ." as a");
		$this->db->where("a.ProductId",$ProductId);
		$this->db->join("color as b","a.ColorId = b.ColorId");
		$result = $query->get()->result_array();
		return $result;
	}

	public function get_product_sizes($ProductId)
	{
		$query = $this->db->select("*");
		$this->db->from("size as a");	//$this->tableName .
		$this->db->where("ProductId",$ProductId);
		// $this->db->where("Status",0);
		$result = $query->get()->result_array();
		return $result;
	}

	public function update_product_color($ProductColorData)
	{
		$query = $this->db->where('ProductColorId',$ProductColorData['ProductColorId']);
		$this->db->update($this->tableName , $ProductColorData);

		return $query->affected_rows();
	}

	public function get_size_details($id)
	{
		$query = $this->db->select("*");
		$this->db->from("size");	//$this->tableName .
		$this->db->where("SizeId",$id);
		return $query->get()->result_array();
	}

	public function update_product_size($ProductSizeData)
	{
		$query = $this->db->where('SizeId',$ProductSizeData['SizeId']);
		$this->db->update("size" , $ProductSizeData);

		return $query->affected_rows();
	}

	public function save_product_size($data)
	{
		$query = $this->db->select("*")->from("size")->where("ProductId",$data['ProductId'])
							->where("SizeName",$data["SizeName"]);
		$result = $query->get()->result_array();
		if(isset($result) && count($result) > 0){
				$query = $this->db->where('ProductId',$data['ProductId']);
				$this->db->where('SizeName',$data['SizeName']);
				$data['UpdatedDate'] = date("Y-m-d H:i:s");//time();
				$this->db->update("size" , $data);
				return $query->affected_rows();
		}else {
				$this->db->insert("size", $data);
				return $this->db->insert_id();
		}
	}

	public function save_product_color($data)
	{
		$query = $this->db->select("*")->from($this->tableName)->where("ProductId",$data['ProductId'])
							->where("ColorId",$data["ColorId"]);
		$result = $query->get()->result_array();
		if(isset($result) && count($result) > 0){
				$query = $this->db->where('ProductId',$data['ProductId']);
				$this->db->where('ColorId',$data['ColorId']);
				$this->db->update($this->tableName , $data);
				return $query->affected_rows();
		}else {
				$this->db->insert($this->tableName, $data);
				return $this->db->insert_id();
		}
	}

}
