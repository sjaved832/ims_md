<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Live_stock extends CI_Model
{

	public function __construct()
	{
		$this->tableName = 'stock';
		parent::__construct();
		$this->load->database();
	}

	public function get_all_stock()
	{
		$this->db->from($this->tableName . " as a");
		$this->db->where("a.Status",0);
		$this->db->join("product as b","a.ProductId = b.ProductId");
		$this->db->join("size as c","a.SizeId = c.SizeId");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_stock_details($StockId)
	{
		$query = $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("StockId",$StockId);
		$result = $query->get()->result_array();
		if(isset($result[0])){
			return $result[0];
		}
		return null;
	}
	public function get_available_stock( $productId, $colorId ,$SizeId ){

			$query = $this->db->select("*");
			$this->db->from($this->tableName);
			$this->db->where("ProductId",$productId);
			$this->db->where("ColorId",$colorId);
			$this->db->where("SizeId",$SizeId);
			$result = $query->get()->result_array();
			if(isset($result[0])){
				return $result[0];
			}
			return null;
	}

	public function update_stock($StockData)
	{
		$user_id = $this->ion_auth->user()->row()->user_id;

		$query = $this->db->where('StockId',$StockData['StockId']);
		$this->db->update($this->tableName , $StockData);
		if($query->affected_rows() > 0){
			$stock_entry = array();
			$stock_entry["UserId"] = $user_id;
			$stock_entry["Operation"] = "Stock Update";
			$stock_entry["StockId"] = $StockData['StockId'];
			$stock_entry["Quantity"] = $StockData['Quantity'];
			$this->db->insert("stock_entry", $stock_entry);
			$this->db->insert_id();
		}
		return $query->affected_rows();
	}

	public function save_stock($data)
	{
		$query = $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("SizeId",$data['SizeId']);
		$this->db->where("ColorId",$data['ColorId']);
		$this->db->where("ProductId",$data['ProductId']);
		$result = $query->get()->result_array();
		if (!isset($result[0])) {
			$user_id = $this->ion_auth->user()->row()->user_id;
			$this->db->insert($this->tableName, $data);
			$data["StockId"] = $this->db->insert_id();

			$stock_entry = array();
			$stock_entry["UserId"] = $user_id;
			$stock_entry["Operation"] = "New Stock";
			$stock_entry["StockId"] = $data['StockId'];
			$stock_entry["Quantity"] = $data['Quantity'];
			$this->db->insert("stock_entry", $stock_entry);
			$this->db->insert_id();

			return $data["StockId"];
		}else {
			$data['StockId'] = $result[0]['StockId'];
			$data['Quantity'] = $result[0]['Quantity'] + $data['Quantity'];
			return $this->update_stock($data);
		}
	}

public function updateStockAfterSalesOrder($data,$flag=null)
{		$query = $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("StockId",$data['StockId']);
		$result = $query->get()->result_array();
		if (!isset($result[0])) {
		}else {
			$data['StockId'] = $result[0]['StockId'];
			$data['Quantity'] = !isset($flag) ? $result[0]['Quantity'] - $data['Quantity'] : $result[0]['Quantity'] + $data['Quantity'];
			return $this->update_stock($data);
		}

}

}
