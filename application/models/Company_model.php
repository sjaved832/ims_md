<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Company_model extends CI_Model
{

	public function __construct()
	{
		$this->tableName = 'company';
		parent::__construct();
		$this->load->database();
	}

	public function get_company()
	{
  		$this->db->from($this->tableName);
  		$query=$this->db->get();
  		return $query->result();
	}

	public function update_company($CompanyData)
	{
		$query = $this->db->where('CompanyId',$CompanyData['CompanyId']);
		$this->db->update($this->tableName , $CompanyData);

		return $query->affected_rows();
	}

	public function save_company($data)
	{
    $company = $this->get_company();
    if($company == null){
  		$this->db->insert($this->tableName, $data);
  		return $this->db->insert_id();
    }else{
			return $this->update_company($data);
		}
    // return null;
	}

}
