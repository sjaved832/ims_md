<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Color_model extends CI_Model
{

	public function __construct()
	{
		$this->tableName = 'color';
		$this->tableNameSize = 'size';
		parent::__construct();
		$this->load->database();
	}

	public function get_all_sizes()
	{
		$this->db->from($this->tableNameSize);
		$this->db->where("status",0);
		$query=$this->db->get();
		return $query->result();
	}
	public function get_all_colors()
	{
		$this->db->from($this->tableName);
		$this->db->where("status",0);
		$query=$this->db->get();
		return $query->result();
	}

	public function get_color_details($ColorId)
	{
		$query = $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("ColorId",$ColorId);
		$result = $query->get()->result_array();
		if(isset($result[0])){
			return $result[0];
		}
		return null;
	}

	public function update_color($ColorData)
	{
		$query = $this->db->where('ColorId',$ColorData['ColorId']);
		$this->db->update($this->tableName , $ColorData);

		return $query->affected_rows();
	}

	public function save_color($data)
	{
		$this->db->insert($this->tableName, $data);
		return $this->db->insert_id();
	}

}
