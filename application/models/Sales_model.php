<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Sales Order
*/
class Sales_model extends CI_Model
{
	public function __construct()
	{
		$this->tableName = 'sales_order';
		parent::__construct();
		$this->load->database();
	}

	public function get_all_sales()
	{
		$this->db->select("a.* ,b.CustomerName");
		$this->db->from($this->tableName ." as a");
		$this->db->join("customer as b","a.CustomerId = b.CustomerId");
		$this->db->where("a.IsInvoiced",0);
		$this->db->where("a.Status",0);
		$query=$this->db->get();
		return $query->result();
	}

	public function get_all_sales_invoice()
	{
		$this->db->select("a.* ,b.CustomerName");
		$this->db->from($this->tableName ." as a");
		$this->db->join("customer as b","a.CustomerId = b.CustomerId");
		$this->db->where("a.IsInvoiced",1);
		$this->db->where("a.Status",0);
		$query=$this->db->get();
		return $query->result();
	}

	public function get_sales_list($SalesList)
	{
		$this->db->select("a.* ,b.CustomerName");
		$this->db->from($this->tableName ." as a");
		$this->db->join("customer as b","a.CustomerId = b.CustomerId");
		$this->db->where("a.IsInvoiced",0);
		$this->db->where_in("a.SalesOrderId",$SalesList);
		$this->db->where("a.Status",0);
		$query=$this->db->get();
		return $query->result();
	}

	public function generate_invoice_sales($SalesList)
	{
		$data = array();
		foreach ($SalesList as $salesObj) {
			array_push($data , array( "SalesOrderId" => $salesObj , "IsInvoiced" => 1 ) );
		}
		return $this->db->update_batch($this->tableName , $data , "SalesOrderId");
	}

	public function get_sales_details($SalesOrderId)
	{
		$query = $this->db->select("a.* , b.CustomerName , c.* ,d.ProductId,d.SizeId,d.ColorId");
		$this->db->from($this->tableName ." as a");
		$this->db->join("customer as b","a.CustomerId = b.CustomerId");
		$this->db->join("sales_order_entry as c","a.SalesOrderId = c.SalesOrderId");
		$this->db->join("stock as d","c.StockId = d.StockId");
		$this->db->where("a.SalesOrderId",$SalesOrderId);
		$result = $query->get()->result_array();
		$fields = array("Pay","GST","Quantity","StockId","ProductId","SizeId","ColorId","SalesOrderEntryId");
		if( count($result) > 0 ){
			$result[0]['ProductList'] = array();
			foreach ($result as $key => $value) {
				$sampleValue = array();
				foreach ($fields as $key1 => $value1) {
					$sampleValue[$value1] = $value[$value1];
				}
				array_push( $result[0]['ProductList'] , $sampleValue);
			}
			foreach ($fields as $key1 => $value1) {
				unset($result[0][$value1]);
			}
		}
		if(isset($result[0])){
			return $result[0];
		}
		return null;
	}


	public function save_sales_order($data)
	{
		$this->db->insert($this->tableName, $data);
		return $this->db->insert_id();
	}

	public function update_sales($SalesData)
	{
		$query = $this->db->where('SalesOrderId',$SalesData['SalesOrderId']);
		$this->db->update($this->tableName , $SalesData);

		return $query->affected_rows();
	}
}
?>
