<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Customer_model extends CI_Model
{


	public function __construct()
	{
		$this->tableName = 'customer';
		parent::__construct();
		$this->load->database();
	}

	public function get_all_customers()
	{
		$this->db->from($this->tableName);
		$this->db->where("status",0);
		$query=$this->db->get();
		return $query->result();
	}

	public function get_customer_details($CustomerId)
	{
		$query = $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("CustomerId",$CustomerId);
		$result = $query->get()->result_array();
		if(isset($result[0])){
			return $result[0];
		}
		return null;
	}

	public function update_customer($CustomerData)
	{
		$query = $this->db->where('CustomerId',$CustomerData['CustomerId']);
		$this->db->update($this->tableName , $CustomerData);

		return $query->affected_rows();
	}

	public function save_customer($data)
	{
		$this->db->insert($this->tableName, $data);
		return $this->db->insert_id();
	}
}
?>
