<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Brand_model extends CI_Model
{

	public function __construct()
	{
		$this->tableName = 'brand';
		parent::__construct();
		$this->load->database();
	}

	public function get_all_brands()
	{
		$this->db->from($this->tableName);
		$this->db->where("status",0);
		$query=$this->db->get();
		return $query->result();
	}

	public function get_brand_details($BrandId)
	{
		$query = $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("BrandId",$BrandId);
		$result = $query->get()->result_array();
		if(isset($result[0])){
			return $result[0];
		}
		return null;
	}

	public function update_brand($BrandData)
	{
		$query = $this->db->where('BrandId',$BrandData['BrandId']);
		$this->db->update($this->tableName , $BrandData);

		return $query->affected_rows();
	}

	public function save_brand($data)
	{
		$this->db->insert($this->tableName, $data);
		return $this->db->insert_id();
	}

}
