<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Invoice_model extends CI_Model
{

	public function __construct()
	{
		$this->tableName = 'invoice';
		parent::__construct();
		$this->load->database();
	}

	public function get_all_invoices()
	{
		$this->db->from($this->tableName ." as a");
		$this->db->where("a.status",0);
		$this->db->join("customer as b","a.CustomerId = b.CustomerId");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_invoice_details($InvoiceId)
	{
		$query = $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("InvoiceId",$InvoiceId);
		$result = $query->get()->result_array();
		if(isset($result[0])){
			return $result[0];
		}
		return null;
	}

	

}
?>
