<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Product_model extends CI_Model
{

	public function __construct()
	{
		$this->tableName = 'product';
		parent::__construct();
		$this->load->database();
	}

	public function get_all_products()
	{
		$this->db->from($this->tableName ." as a");
		$this->db->where("a.status",0);
		$this->db->join("brand as b","a.BrandId = b.BrandId");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_product_details($ProductId)
	{
		$query = $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("ProductId",$ProductId);
		$result = $query->get()->result_array();
		if(isset($result[0])){
			return $result[0];
		}
		return null;
	}

	public function update_product($ProductData)
	{
		$query = $this->db->where('ProductId',$ProductData['ProductId']);
		$this->db->update($this->tableName , $ProductData);

		return $query->affected_rows();
	}

	public function save_product($data)
	{
		$this->db->insert($this->tableName, $data);
		return $this->db->insert_id();
	}

}
