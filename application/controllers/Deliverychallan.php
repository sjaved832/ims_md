<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deliverychallan extends MY_Controller
{
	public function __construct()
	{
			parent::__construct();

			if (!$this->ion_auth->logged_in())
			{
				//redirect them to the login page
				redirect('auth/login', 'refresh');
			}
			$this->load->helper('url');
	}

	public function index()
	{
		$this->render_page('sales/deliverychallan_list');
	}
	public function deliverychallan_view()
	{
		$this->render_page('sales/deliverychallan_view');
	}
	public function add_deliverychallan()
	{
		$this->render_page('sales/add_deliverychallan');
	}
}
?>
