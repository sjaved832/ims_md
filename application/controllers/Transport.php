<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transport extends MY_Controller
{
	public function __construct()
	{
			parent::__construct();

			if (!$this->ion_auth->logged_in())
			{
				//redirect them to the login page
				redirect('auth/login', 'refresh');
			}
			//$this->load->helper('url');
	}

	public function index()
	{
		$this->render_page('core/transport_view');
	}
}
?>
