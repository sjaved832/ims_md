<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salesorder extends MY_Controller
{
	public function __construct()
	{
			parent::__construct();

			if (!$this->ion_auth->logged_in())
			{
				//redirect them to the login page
				redirect('auth/login', 'refresh');
			}
			$this->load->helper('url');
	}

	public function index()
	{
		$this->render_page('sales/salesorder');
	}

	public function add_salesorder()
	{
		$this->render_page('sales/add_salesorder');
	}

	public function edit_salesorder($id=null)
	{
		if(!isset($id)){
			$id = $this->uri->segment(3);
		}
		if(!isset($id)){
			 redirect( "salesorder" , 'refresh');
		}
		$this->data['SalesOrderId'] = $id ;
		$this->render_page('sales/edit_salesorder');
	}
}
?>
