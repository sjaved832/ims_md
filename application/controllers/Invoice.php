<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends MY_Controller
{
	public function __construct()
	{
			parent::__construct();

			if (!$this->ion_auth->logged_in())
			{
				//redirect them to the login page
				redirect('auth/login', 'refresh');
			}
			$this->load->helper('url');
	}

	public function index()
	{
		$this->render_page('sales/invoice_list');
	}
	public function view_invoice($id = null)
	{
		$this->render_page('sales/invoice');
	}
	public function edit_invoice($id=null)
	{

	}

}

 ?>
