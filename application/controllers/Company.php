<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends MY_Controller
{
	public function __construct()
	{
			parent::__construct();

			if (!$this->ion_auth->logged_in())
			{
				//redirect them to the login page
				redirect('auth/login', 'refresh');
			}
      $this->load->model("company_model");
			$this->load->helper('url');
	}

	public function index()
	{

		$this->render_page('core/company_view');
	}

  public function add_company()
	{
    if( $this->company_model->get_company() != null )
      redirect('dashboard', 'refresh');
    else{
			$this->headerData["isDisable"] = TRUE;
			$this->render_page('core/company_view');
    }
	}
}
?>
