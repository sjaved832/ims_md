
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

class User extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->result = array("status"=>true,"message"=>"success","data"=>array());
    }

    function all_users_get()
    {
            $result = $this->result;
            $data = $this->ion_auth->users()->result();
            if(isset($data) && $data != null){
              $result['status'] = TRUE;
              $result['message'] = "Successfully fetch user.";
              $result['data'] = $data;
            }else {
              $result['status'] = FALSE;
              $result['message'] = "No such user Id";
              $result['data'] = $data;
            }
            $this->response($result);
    }

    function change_password_post()
    {
      $result = $this->result;

			$identity = $this->session->userdata('identity');
      $new = $this->input->post('new_password');
      $old = $this->input->post('old_password');

      if ($this->ion_auth->change_password($identity, $old, $new))
       {
         $result['message'] = 'Password change successful';
           // $this->set_message('password_change_successful');
       }else {
         $result['message'] = 'Password change un-successful';
       }
      $this->response($result);
    }
}
