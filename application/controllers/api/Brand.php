
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

class Brand extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //Result format
        $this->result = array("status"=>true,"message"=>"success","data"=>array());

        // Load Models
		    $this->load->model('brand_model');
    }
    function brand_details_get()
    {
      $cust_id = $this->uri->segment(4);
      $data = $this->brand_model->get_brand_details($cust_id);

      $result = $this->result;

      if(isset($data) && $data != null){
        $result['status'] = TRUE;
        $result['message'] = "successfully fetch brand Details";
        $result['data'] = $data;
      }else {
        $result['status'] = FALSE;
        $result['message'] = "No such brand Id";
        $result['data'] = $data;
      }
      $this->response($result);
    }

    function all_brands_get()
    {
      $result = $this->result;
      $result['status'] = TRUE;
      $result['message'] = "successfully fetch brand data";
      $result['data'] = $this->brand_model->get_all_brands();
		  $this->response($result);
	   }

     function save_brand_post()
     {
       $data = array();
       $data['BrandName'] = $this->input->post("BrandName");

       $result = $this->result;
       $result['data'] = $this->brand_model->save_brand($data);
 		  $this->response($result);
      }

      function brand_update_post()
      {
          $BrandData = $this->input->post();
          $data = $this->brand_model->update_brand($BrandData);
          // if($data)
          $result = $this->result;
          $result['status'] = TRUE;
          $result['message'] = "successfully fetch brand Updated";
    		  $this->response($result);
      }
}


/*$config = [
    [
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required|min_length[3]|alpha_dash',
            'errors' => [
                    'required' => 'We need both username and password',
                    'min_length'=>'Minimum Username length is 3 characters',
                    'alpha_dash'=>'You can just use a-z 0-9 _ . – characters for input',
            ],
    ],
    [
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|min_length[6]',
            'errors' => [
                    'required' => 'You must provide a Password.',
                    'min_length'=>'Minimum Password length is 6 characters',
            ],
    ],
];

$data = $this->inpu->get();

$this->form_validation->set_data($data);
$this->form_validation->set_rules($config);

if($this->form_validation->run()==FALSE){
    print_r($this->form_validation->error_array());
*/
?>
