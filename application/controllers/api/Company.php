
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

class Company extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //Result format
        $this->result = array("status"=>true,"message"=>"success","data"=>array());

  			if (!$this->ion_auth->logged_in())
  			{
          $this->result['status'] = false;
          $this->result['message'] = "User needs to login";
          $this->response($this->result);
          exit;
        }
        // Load Models
		    $this->load->model('company_model');
    }

    function company_details_get()
    {
      $cust_id = $this->uri->segment(4);
      $data = $this->company_model->get_company_details($cust_id);

      $result = $this->result;

      if(isset($data) && $data != null){
        $result['status'] = TRUE;
        $result['message'] = "successfully fetch Company Details";
        $result['data'] = $data;
      }else {
        $result['status'] = FALSE;
        $result['message'] = "No such Company Id";
        $result['data'] = $data;
      }
      $this->response($result);
    }

    function all_companys_get()
    {
      $result = $this->result;
      $result['status'] = TRUE;
      $result['message'] = "successfully fetch Company data";
      $result['data'] = $this->company_model->get_company();
		  $this->response($result);
	   }

     function save_company_post()
     {
       $data = $this->input->post();
       $result = $this->result;
       try {
         $result['data'] = $this->company_model->save_company($data);
       } catch (\Exception $e) {
         $result['status'] = false;
         $result['message'] =$e;
       }
       $this->response($result);
      }

      function company_update_post()
      {
          $CompanyData = $this->input->post();
          $data = $this->company_model->update_company($CompanyData);
          $result = $this->result;
          $result['status'] = TRUE;
          $result['message'] = "successfully fetch Company Updated";
    		  $this->response($result);
      }
}
?>
