<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

class ProductColor extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //Result format
        $this->result = array("status"=>true,"message"=>"success","data"=>array());

        // Load Models
		    $this->load->model('product_color_model');
    }
    function product_color_details_get()
    {
      $cust_id = $this->uri->segment(4);
      $data = $this->product_color_model->get_product_color_details($cust_id);

      $result = $this->result;

      if(isset($data) && $data != null){
        $result['status'] = TRUE;
        $result['message'] = "successfully fetch product_color Details";
        $result['data'] = $data;
      }else {
        $result['status'] = FALSE;
        $result['message'] = "No such product_color Id";
        $result['data'] = $data;
      }
      $this->response($result);
    }

    function all_product_colors_get()
    {
      $result = $this->result;
      $result['status'] = TRUE;
      $result['message'] = "successfully fetch product_color data";
      $result['data'] = $this->product_color_model->get_all_product_colors();
		  $this->response($result);
	   }

     function product_colors_get($ProductId){
       $result = $this->result;
       $result['status'] = TRUE;
       $result['message'] = "successfully fetch product_color data";
       $result['data'] = $this->product_color_model->get_product_colors($ProductId);
   		 $this->response($result);
     }

	 function product_sizes_get($ProductId){
       $result = $this->result;
       $result['status'] = TRUE;
       $result['message'] = "successfully fetch product_color data";
       $result['data'] = $this->product_color_model->get_product_sizes($ProductId);
   		 $this->response($result);
     }

	 function save_product_size_post()
     {
       $data = $this->input->post();

       $result = $this->result;
       $result['data'] = $this->product_color_model->save_product_size($data);
 		  $this->response($result);
      }

     function save_product_color_post()
     {
       $data = $this->input->post();

       $result = $this->result;
       $result['data'] = $this->product_color_model->save_product_color($data);
 		  $this->response($result);
      }

      function product_color_update_post()
      {
          $ColorData = $this->input->post();
          $data = $this->product_color_model->update_product_color($ColorData);

          $result = $this->result;
          $result['status'] = TRUE;
          $result['message'] = "successfully fetch product_color Updated";
    		  $this->response($result);
      }

	   function product_size_update_post()
      {
          $SizeData = $this->input->post();
          $data = $this->product_color_model->update_product_size($SizeData);

          $result = $this->result;
          $result['status'] = TRUE;
          $result['message'] = "successfully fetch product_color Updated";
    		  $this->response($result);
      }
      public function size_details_get($id){
         $id = $this->uri->segment(4);
         $data = $this->product_color_model->get_size_details($id);

        $result = $this->result;
        $result['status'] = TRUE;
        $result['message'] = "successfully fetch product_color Updated";
        $result['data'] = $data[0];
        $this->response($result);
      }
}
?>
