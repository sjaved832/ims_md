

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

class Transport extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //Result format
        $this->result = array("status"=>true,"message"=>"success","data"=>array());

        // Load Models
		    $this->load->model('transport_model');
    }
    function transport_details_get()
    {
      $cust_id = $this->uri->segment(4);
      $data = $this->transport_model->get_transport_details($cust_id);

      $result = $this->result;

      if(isset($data) && $data != null){
        $result['status'] = TRUE;
        $result['message'] = "successfully fetch transport Details";
        $result['data'] = $data;
      }else {
        $result['status'] = FALSE;
        $result['message'] = "No such transport Id";
        $result['data'] = $data;
      }
      $this->response($result);
    }

    function all_transports_get()
    {
      $result = $this->result;
      $result['status'] = TRUE;
      $result['message'] = "successfully fetch transport data";
      $result['data'] = $this->transport_model->get_all_transports();
		  $this->response($result);
	   }

     function save_transport_post()
     {
       $data = array();
       $data['transportName'] = $this->input->post("transportName");

       $result = $this->result;
       $result['data'] = $this->transport_model->save_transport($data);
 		  $this->response($result);
      }

      function transport_update_post()
      {
          $TransportData = $this->input->post();
          $data = $this->transport_model->update_transport($TransportData);
          // if($data)
          $result = $this->result;
          $result['status'] = TRUE;
          $result['message'] = "successfully fetch transport Updated";
    		  $this->response($result);
      }
}
?>
