
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

class Agent extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //Result format
        $this->result = array("status"=>true,"message"=>"success","data"=>array());

        // Load Models
		    $this->load->model('agent_model');
    }

    function agent_details_get()
    {
      $cust_id = $this->uri->segment(4);
      $data = $this->agent_model->get_agent_details($cust_id);

      $result = $this->result;

      if(isset($data) && $data != null){
        $result['status'] = TRUE;
        $result['message'] = "successfully fetch Agent Details";
        $result['data'] = $data;
      }else {
        $result['status'] = FALSE;
        $result['message'] = "No such Agent Id";
        $result['data'] = $data;
      }
      $this->response($result);
    }

    function all_agents_get()
    {
      $result = $this->result;
      $result['status'] = TRUE;
      $result['message'] = "successfully fetch Agent data";
      $result['data'] = $this->agent_model->get_all_agents();
		  $this->response($result);
	   }

     function save_agent_post()
     {
       $data = $this->input->post();//array();
       // $data['AgentName'] = $this->input->post("AgentName");
       // $data['MobileNumber'] = $this->input->post("MobileNumber");
       // $data['Email'] = $this->input->post("Email");

       $result = $this->result;
       try {
         $result['data'] = $this->agent_model->save_agent($data);
       } catch (\Exception $e) {
         $result['status'] = false;
         $result['message'] =$e;
       }
       $this->response($result);

      }

      function agent_update_post()
      {
          $AgentData = $this->input->post();
          $data = $this->agent_model->update_agent($AgentData);
          // if($data)
          $result = $this->result;
          $result['status'] = TRUE;
          $result['message'] = "successfully fetch Agent Updated";
          // $result['data'] = $data ;//this->agent_model->save_agent($data);
    		  $this->response($result);
      }
}
?>
