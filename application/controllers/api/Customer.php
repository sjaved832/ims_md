
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

class Customer extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //Result format
        $this->result = array("status"=>true,"message"=>"success","data"=>array());

        // Load Models
		    $this->load->model('customer_model');
    }

    function customer_details_get()
    {
      $cust_id = $this->uri->segment(4);
      $data = $this->customer_model->get_customer_details($cust_id);

      $result = $this->result;

      if(isset($data) && $data != null){
        $result['status'] = TRUE;
        $result['message'] = "successfully fetch Customer Details";
        $result['data'] = $data;
      }else {
        $result['status'] = FALSE;
        $result['message'] = "No such Customer Id";
        $result['data'] = $data;
      }
      $this->response($result);
    }

    function all_customers_get()
    {
      $result = $this->result;
      $result['status'] = TRUE;
      $result['message'] = "successfully fetch Customer data";
      $result['data'] = $this->customer_model->get_all_customers();
		  $this->response($result);
	   }

     function save_customer_post()
     {
       $data = $this->input->post();//array();
       // $data['CustomerName'] = $this->input->post("CustomerName");
       // $data['MobileNumber'] = $this->input->post("MobileNumber");
       // $data['Email'] = $this->input->post("Email");

       $result = $this->result;
       try {
         $result['data'] = $this->customer_model->save_customer($data);
       } catch (\Exception $e) {
         $result['status'] = false;
         $result['message'] =$e;
       }
       $this->response($result);

      }

      function customer_update_post()
      {
          $CustomerData = $this->input->post();
          $data = $this->customer_model->update_customer($CustomerData);
          // if($data)
          $result = $this->result;
          $result['status'] = TRUE;
          $result['message'] = "successfully fetch Customer Updated";
          // $result['data'] = $data ;//this->customer_model->save_customer($data);
    		  $this->response($result);
      }
}


/*$config = [
    [
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required|min_length[3]|alpha_dash',
            'errors' => [
                    'required' => 'We need both username and password',
                    'min_length'=>'Minimum Username length is 3 characters',
                    'alpha_dash'=>'You can just use a-z 0-9 _ . – characters for input',
            ],
    ],
    [
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|min_length[6]',
            'errors' => [
                    'required' => 'You must provide a Password.',
                    'min_length'=>'Minimum Password length is 6 characters',
            ],
    ],
];

$data = $this->inpu->get();

$this->form_validation->set_data($data);
$this->form_validation->set_rules($config);

if($this->form_validation->run()==FALSE){
    print_r($this->form_validation->error_array());
*/
?>
