<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

class Sale extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Result format
        $this->result = array("status"=>true,"message"=>"success","data"=>array());
        // Load Models
		    $this->load->model('sales_model');
        $this->load->model('sales_entry_model');
        $this->load->model('live_stock');
    }

    function save_sales_order_post()
    {
       $data = $this->input->post();
       $result = $this->result;
       // $data['main'] = array();
       try {
         if (count($data['StockId']) < 1) {
           throw new \Exception("Error Processing Request", 1);
         }
         $SalesOrderData = array(
           'CustomerId' => $data['CustomerId'],
           'Discount' => $data['discount'],
           'TotalPay' => $data['TotalPay']
         );

         $SalesOrderId = $this->sales_model->save_sales_order($SalesOrderData);
         for ($i=0; $i < count($data['StockId']); $i++) {
             if ($data['Valid'][$i] == "true") {
               $SalesOrderEntry = array(
                 "SalesOrderId" => $SalesOrderId,
                 "StockId" => $data['StockId'][$i],
                 "Quantity" => $data['Quantity'][$i],
                 "GST" => $data['GST'][$i],
                 "Pay" => $data['total_price'][$i]
               );
               $this->live_stock->updateStockAfterSalesOrder(array("StockId" => $data['StockId'][$i],"Quantity" => $data['Quantity'][$i]));
               $SalesOrderEntry['Done'] = $this->sales_entry_model->save_sales_order_entry($SalesOrderEntry);
            }
         }
         $result['data'] = null;
         $this->response($result);
       } catch (\Exception $e) {
         $result["status"] = false;
         $result["message"] = "Please select alteast one row";

         $this->response($result);

       }
     }

     function return_sales_order_post()
     {
       // $SalesOrderId = $this->input->get("SalesOrderId");
       $data = $this->input->post();
       for ($i=0; $i < count($data['StockId']); $i++) {
         $StockId = $this->sales_entry_model->find_stock_entry($data['CustomerId'],$data['StockId'][$i],$data['Quantity'][$i]);
          //  if ($data['Valid'][$i] == "true") {
          // if(isset($StockId)){
          //    $SalesOrderEntry = array(
          //      "SalesOrderId"=> $data['SalesOrderId'],
          //     //  "SalesOrderEntryRefId" => $data['SalesOrderEntryId'][$i],s
          //      "StockId" => $StockId,
          //      "Quantity" => $data['Quantity'][$i],
          //     //  "GST" => $data['GST'][$i],
          //     //  "Pay" => $data['total_price'][$i]
          //    );
          //    $this->live_stock->updateStockAfterSalesOrder($SalesOrderEntry , true);
          //    $SalesOrderEntry['Done'] = $this->sales_entry_model->save_sales_order_entry($SalesOrderEntry);
          // }
       }

       $result = $this->result;
       $result['data'] = $StockId;//$data;
       $this->response($result);

    }
    function sales_details_get()
    {
      $id = $this->uri->segment(4);
      $data = $this->sales_model->get_sales_details($id);

      $result = $this->result;

      if(isset($data) && $data != null){
        $result['status'] = TRUE;
        $result['message'] = "successfully fetch stock Details";
        $result['data'] = $data;
      }else {
        $result['status'] = FALSE;
        $result['message'] = "No such stock Id";
        $result['data'] = $data;
      }
      $this->response($result);
    }

    function all_sales_get()
    {
      $result = $this->result;
      $result['status'] = TRUE;
      $result['message'] = "successfully fetch stock data";
      $result['data'] = $this->sales_model->get_all_sales();
		  $this->response($result);
	   }

     public function generate_invoice_sales_post(){
       $result = $this->result;
       $result['status'] = TRUE;
       $result['message'] = "successfully fetch stock data";
       $result['data'] = $this->sales_model->generate_invoice_sales($this->input->post("SelectedSalesId"));
      $this->response($result);
     }

     public function fetch_sales_post()
     {
         $result = $this->result;
         $result['status'] = TRUE;
         $result['message'] = "successfully fetch stock data";
         $result['data'] = $this->sales_model->get_sales_list($this->input->post("SelectedSalesId"));
   		  $this->response($result);
     }

      public function all_invoice_sales_get()
      {
          $result = $this->result;
          $result['status'] = TRUE;
          $result['message'] = "Successfully fetch sales data";
          $result['data'] = $this->sales_model->get_all_sales_invoice();
    		  $this->response($result);
      }

     function sales_update_post()
      {
          $SalesData = $this->input->post();
          $data = $this->sales_model->update_sales($SalesData);

          $result = $this->result;
          $result['status'] = TRUE;
          $result['message'] = "successfully fetch product Updated";
          $result['data'] = $SalesData["SalesOrderId"];
          $this->response($result);
      }


      function stock_update_post()
      {
          $StockData = $this->input->post();
          $data = $this->live_stock->update_stock(array('StockId' => $StockData["StockId"], "Quantity" => $StockData["Quantity"]));

          $result = $this->result;
          $result['status'] = TRUE;
          $result['message'] = "successfully fetch stock Updated";
          $result['data'] = $data;//$StockData["StockId"];
    		  $this->response($result);
      }
}
?>
