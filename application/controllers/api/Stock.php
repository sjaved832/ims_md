<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

class Stock extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Result format
        $this->result = array("status"=>true,"message"=>"success","data"=>array());

        // Load Models
		    $this->load->model('live_stock');
    }

    function stock_details_get()
    {
      $id = $this->uri->segment(4);
      $data = $this->live_stock->get_stock_details($id);

      $result = $this->result;

      if(isset($data) && $data != null){
        $result['status'] = TRUE;
        $result['message'] = "successfully fetch stock Details";
        $result['data'] = $data;
      }else {
        $result['status'] = FALSE;
        $result['message'] = "No such stock Id";
        $result['data'] = $data;
      }
      $this->response($result);
    }

        function available_stock_get()
        {
          $productId = $this->input->get("ProductId");
          $colorId = $this->input->get("ColorId");
          $SizeId = $this->input->get("SizeId");
          $quantity = $this->input->get("Quantity");
          $data = $this->live_stock->get_available_stock( $productId, $colorId ,$SizeId  );
          // $data = $this->input->get();
          $result = $this->result;

          if(isset($data) && $data != null){
            $result['status'] = TRUE;
            $result['message'] = "successfully fetch stock Details";
            $result['data'] = $data;
          }else {
            $result['status'] = FALSE;
            $result['message'] = "No such stock";
            $result['data'] = $data;
          }
          $this->response($result);
        }
    function all_stocks_get()
    {
      $result = $this->result;
      $result['status'] = TRUE;
      $result['message'] = "successfully fetch stock data";
      $result['data'] = $this->live_stock->get_all_stock();
		  $this->response($result);
	   }

     function save_stock_post()
     {
        $data = $this->input->post();
        $result = $this->result;
        $result['data'] = $this->live_stock->save_stock($data);
 		    $this->response($result);
      }

      function stock_update_post()
      {
          $StockData = $this->input->post();
          $data = $this->live_stock->update_stock(array('StockId' => $StockData["StockId"], "Quantity" => $StockData["Quantity"]));

          $result = $this->result;
          $result['status'] = TRUE;
          $result['message'] = "successfully fetch stock Updated";
          $result['data'] = $data;//$StockData["StockId"];
    		  $this->response($result);
      }
}
?>
