
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

class Color extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //Result format
        $this->result = array("status"=>true,"message"=>"success","data"=>array());

        // Load Models
		    $this->load->model('color_model');
    }
    function color_details_get()
    {
      $cust_id = $this->uri->segment(4);
      $data = $this->color_model->get_color_details($cust_id);

      $result = $this->result;

      if(isset($data) && $data != null){
        $result['status'] = TRUE;
        $result['message'] = "successfully fetch color Details";
        $result['data'] = $data;
      }else {
        $result['status'] = FALSE;
        $result['message'] = "No such color Id";
        $result['data'] = $data;
      }
      $this->response($result);
    }


    function all_sizes_get()
    {
      $result = $this->result;
      $result['status'] = TRUE;
      $result['message'] = "successfully fetch size data";
      $result['data'] = array('S','M','L','XL','XXL','XXXL','XXXXL' , 'XXXXXL','XXXXXXL');
        //$this->color_model->get_all_sizes();
		  $this->response($result);
	}

    function all_colors_get()
    {
      $result = $this->result;
      $result['status'] = TRUE;
      $result['message'] = "successfully fetch color data";
      $result['data'] = $this->color_model->get_all_colors();
		  $this->response($result);
	   }

     function save_color_post()
     {
       $data = array();
       $data['ColorName'] = $this->input->post("ColorName");

       $result = $this->result;
       $result['data'] = $this->color_model->save_color($data);
 		  $this->response($result);
      }

      function color_update_post()
      {
          $ColorData = $this->input->post();
          $data = $this->color_model->update_color($ColorData);

          $result = $this->result;
          $result['status'] = TRUE;
          $result['message'] = "successfully fetch color Updated";
    		  $this->response($result);
      }
}
?>
