<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

class Product extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        // Result format
        $this->result = array("status"=>true,"message"=>"success","data"=>array());

        // Load Models
		    $this->load->model('product_model');
    }

    function product_details_get()
    {
      $id = $this->uri->segment(4);
      $data = $this->product_model->get_product_details($id);

      $result = $this->result;

      if(isset($data) && $data != null){
        $result['status'] = TRUE;
        $result['message'] = "successfully fetch product Details";
        $result['data'] = $data;
      }else {
        $result['status'] = FALSE;
        $result['message'] = "No such product Id";
        $result['data'] = $data;
      }
      $this->response($result);
    }

    function all_products_get()
    {
      $result = $this->result;
      $result['status'] = TRUE;
      $result['message'] = "successfully fetch product data";
      $result['data'] = $this->product_model->get_all_products();
		  $this->response($result);
	   }

     function save_product_post()
     {
        $data = $this->input->post();
        $result = $this->result;
        $result['data'] = $this->product_model->save_product($data);
 		    $this->response($result);
      }

      function product_update_post()
      {
          $ProductData = $this->input->post();
          $data = $this->product_model->update_product($ProductData);

          $result = $this->result;
          $result['status'] = TRUE;
          $result['message'] = "successfully fetch product Updated";
          $result['data'] = $ProductData["ProductId"]; 
    		  $this->response($result);
      }
}
?>
