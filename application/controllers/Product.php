<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller
{
	public function __construct()
	{
			parent::__construct();

			if (!$this->ion_auth->logged_in())
			{
				//redirect them to the login page
				redirect('auth/login', 'refresh');
			}
			$this->load->helper('url');
	}

	public function index()
	{
		$this->render_page('product/product_view');
	}
	public function add_product()
	{
		$this->render_page('product/add_product');
	}
	public function edit_product($id=null)
	{
		if(!isset($id)){
			$id = $this->uri->segment(3);
		}
		if(!isset($id)){
			 redirect( "product" , 'refresh');
		}
		$this->data = array('ProductId' => $id );
		$this->render_page('product/add_product');
	}
}

 ?>
