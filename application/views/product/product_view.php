<section class="content">
    <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                PRODUCTS
                            </h2>
                            <a href="<?=base_url('product/add_product')?>" type="button" class="btn btn-primary m-t-15 waves-effect " style="float:right; margin-top: -22px;">ADD NEW</a>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>Product ID</th>
                                            <th>Product Code</th>
                                            <th>Product Description</th>
                                            <th>Product Brand</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                      <tr>
                                        <th>Product ID</th>
                                        <th>Product Code</th>
                                        <th>Product Description</th>
                                        <th>Product Brand</th>
                                        <th>Action</th>
                                      </tr>
                                    </tfoot>

                                    <tbody id="product_table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <div class="modal fade" id="customModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Product</h4>
                </div>
                <div class="modal-body">
                  <div class="row clearfix">
                                <div class="col-sm-12">

                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Product Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Product">
                                        </div>
                                    </div>  <div class="form-group">
                                          <div class="form-line">
                                              <input type="text" class="form-control" placeholder="Product">
                                          </div>
                                      </div>  <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" placeholder="Product">
                                            </div>
                                        </div>
                                </div>
                            </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect">SAVE </button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</section>

 <!-- Jquery DataTable Plugin Js -->
 <script src="<?php echo base_url('plugins/jquery-datatable/jquery.dataTables.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/jszip.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/pdfmake.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/vfs_fonts.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.print.min.js')?>"></script>


   <script type="text/javascript">
   $(document).ready( function () {


     // Reload Data Table after reload
       fetchProducts().then(reloadDataTable);
       //
       // $("#form").validate({
     	// 	submitHandler:function(form)
     	// 	{
     	//      save(form);
       //      return false;
     	// 	},
     	// 	errorElement: 'span',
     	// 	errorClass: 'help-block error',
     	// 	rules:
     	// 	{
     	// 		CustomerName:"required",
     	// 		CustomerArea:"required"
     	// 	},
     	// 	messages:
     	// 	{	CustomerName:"Please Enter Customer Name",
     	// 		CustomerArea:"Please Enter Customer Area"
     	// 	},
     	// 	ignore: []
     	// });
 });

   function reloadDataTable() {
     setTimeout(function () {
       $('.dataTable').DataTable();
     },100)
   }


     function fetchProducts() {
       return $.ajax({
         url : "<?php echo site_url('/api/product/all_products/')?>",
         type: "GET",
         dataType: "JSON"
       }).then(function (data) {
         if (data.status) {
           $("#product_table").empty();
           data.data.forEach((custObj)=>{
             var tr = $('<tr/>');
             tr.append("<td>" + custObj.ProductId + "</td>");
             tr.append("<td>" + custObj.ProductCode + "</td>");
             tr.append("<td>" + custObj.ProductDescription + "</td>");
             tr.append("<td>" + custObj.BrandName + "</td>");

             tr.append('<td>' +
             '<button class="btn btn-warning" onclick="edit_product('+custObj.ProductId+')"><i class="glyphicon glyphicon-pencil"></i></button>'+
             '<button class="btn btn-danger" onclick="delete_product('+custObj.ProductId+')"><i class="glyphicon glyphicon-remove"></i></button>'+
             '</td>');
             $("#product_table").append(tr);
           });
         }
         return Promise.resolve(data);
       });
     }

     function edit_product(id) {
       window.location.href = baseUrl + "product/edit_product/"+id;
     }

     function delete_product(id)
     {
       if(confirm('Are you sure delete this data?'))
       {
         // ajax delete data from database
           $.ajax({
             url : "<?php echo site_url('/api/product/product_update')?>/"+id,
             type: "POST",
             data :{
               ProductId : id,
               status : 1
             },
             dataType: "JSON",
             success: function(data)
             {
               fetchProducts();
                // location.reload();
             },
             error: function (jqXHR, textStatus, errorThrown)
             {
                 alert('Error deleting data');
             }
         });

       }
     }
     </script>
