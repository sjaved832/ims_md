<style>
.open > .dropdown-menu {
    display: block;
    overflow: initial !important;
}
.bootstrap-select{
  padding: 2px !important;
}
</style>
<script type="text/javascript">
  <?php
  if(!isset($ProductId)){
    $ProductId = "null";
  }
  ?>
  var ProductId = <?=$ProductId; ?>;

  function gotoProduct() {
    window.location.href = baseUrl +"product";
  }
</script>
<section class="content">
    <div class="container-fluid">
      <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">
                  <div class="header">
                      <h2>Product and colors</h2>
                      <ul class="header-dropdown m-r--5">
                          <li class="dropdown">
                              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                  <i class="material-icons">more_vert</i>
                              </a>
                              <ul class="dropdown-menu pull-right">
                                  <li><a href="javascript:void(0);" onclick="gotoProduct();">Cancel</a></li>
                                  <li><a href="javascript:void(0);">Another action</a></li>
                                  <li><a href="javascript:void(0);">Something else here</a></li>
                              </ul>
                          </li>
                      </ul>
                  </div>
                  <div class="body">
                      <form id="product_form" method="POST">
                          <h3> Product Information</h3>
                          <fieldset>
                                <input type="hidden" id="ProductId" name="ProductId" value="">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" id="ProductCode" class="form-control" name="ProductCode" required>
                                        <label class="form-label" style="top: -10px;">Product Code</label>
                                    </div>
                                </div>
                                <div class="">
                                    <!-- form-group form-float <div class="col-md-3">-->
                                    <p>
                                        <b>Product Brand</b>
                                    </p>
                                    <select id="BrandId" name="BrandId" class="form-control">
                                        <option>---- Please wait ----</option>
                                    </select>

                                </div>
                                <div class="form-group form-float" style="padding-top: 16px;">
                                    <div class="form-line">
                                        <textarea name="ProductDescription" id="ProductDescription" cols="30" rows="5" class="form-control no-resize" required></textarea>
                                        <label class="form-label" style="top: -10px;">Product Description</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" id="checkbox" name="checkbox">
                                    <label for="checkbox">I have read and accept the terms</label>
                                </div>
                                <div>

                              </div>
                                <!-- <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button> -->
                          </fieldset>



                          <h3>Size Information</h3>
                          <fieldset >
                            <div class="" >
                              <table>
                                <thead>
                                    <tr>
                                      <th width="20%" style="padding-bottom: 24px;" >Size</th>
                                      <th width="80%" style="padding-bottom: 24px;">Price</th>
                                    </tr>
                                </thead>
                                <tbody id="SizeList">
                                  <tr>
                                    <td>
                                      <div class="form-group">
                                        <div class="">
                                              <input type="checkbox" class='ProductSize' data="S" id="ProductSize" name="checkbox">
                                              <label for="ProductSize">S</label>
                                        </div>
                                      </div>
                                    </td>
                                    <td>
                                    <div class="form-group">
                                      <div  class="form-line">
                                          <input type="number" data="S" id="ProductPrice" class="form-control" required name="ProductPrice_S">
                                          <label class="form-label"  style="top: -8px;">Product Price</label>
                                      </div>
                                    </div>
                                    </td>
                                  </tr>

                                </tbody>
                              </table>
                            </div>
                          </fieldset>

						              <h3>Color Information</h3>
                          <fieldset >
                            <div class="" id="ColorList">

                            </div>
                          </fieldset>
<!--
                          <h3>Terms & Conditions - Finish</h3>
                          <fieldset>
                              <input id="acceptTerms-2" name="acceptTerms" type="checkbox" required>
                              <label for="acceptTerms-2">I agree with the Terms and Conditions.</label>
                          </fieldset> -->
                      </form>
                  </div>
              </div>
            </div>
      </div>

    </div>
</section>

<!-- JQuery Steps Plugin Js -->
<script src="<?=base_url('plugins/jquery-steps/jquery.steps.js')?>"></script>

<!-- Sweet Alert Plugin Js -->
<script src="<?=base_url('plugins/sweetalert/sweetalert.min.js')?>"></script>

<script src="<?=base_url('js/product.js')?>"></script>


<script type="text/javascript">

$(document).ready( function () {
    // tabLoad()
    fetchBrands(); //Comes from product
  })
  </script>


    <script type="text/javascript">
    $(document).ready( function () {
        if( (ProductId != undefined || ProductId != null ) && typeof ProductId == "number"){
          loadProduct( ProductId ).then(function ( data ) {
          })
        }
    });
    </script>
