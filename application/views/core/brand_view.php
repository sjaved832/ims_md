<section class="content">
    <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                BRANDS
                            </h2>
                            <button type="button" onclick="add_brand()" class="btn btn-primary m-t-15 waves-effect" style="float:right; margin-top: -22px;">ADD NEW</button>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>Brand ID</th>
                                            <th>Brand Name</th>

                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                      <tr>
                                          <th>Brand ID</th>
                                          <th>Brand Name</th>

                                          <th>Action</th>
                                      </tr>
                                    </tfoot>

                                    <tbody id="brand_table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <div class="modal fade" id="customModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Brands</h4>
                </div>
                <form id="form_validation" action="#">
                        <div class="modal-body">
                  <input type="hidden" value="" name="BrandId"/>
                          <div class="row clearfix">
                                        <div class="col-sm-12">

                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="BrandName" required placeholder="Brand Name">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-link waves-effect" onclick="saveBrand()">SAVE </button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                </form>

            </div>
        </div>
    </div>
</section>

 <!-- Jquery DataTable Plugin Js -->
 <script src="<?php echo base_url('plugins/jquery-datatable/jquery.dataTables.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/jszip.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/pdfmake.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/vfs_fonts.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.print.min.js')?>"></script>


 <script type="text/javascript">
$(document).ready( function () {
     fetchBrands().then(reloadDataTable);
});

function saveBrand() {
 var valid = $("#form_validation").valid();
 if (valid) {
   save();
 }
}

 function reloadDataTable() {
   setTimeout(function () {
     $('.dataTable').DataTable();
   },100)
 }
   var save_method; //for save method string
   var table;

   function fetchBrands() {
     return $.ajax({
       url : "<?php echo site_url('/api/brand/all_brands/')?>",
       type: "GET",
       dataType: "JSON"
     }).then(function (data) {
       if (data.status) {
         $("#brand_table").empty();
         data.data.forEach((custObj)=>{
           var tr = $('<tr/>');
           tr.append("<td>" + custObj.BrandId + "</td>");
           tr.append("<td>" + custObj.BrandName + "</td>");
           tr.append('<td>' +
           '<button class="btn btn-warning" onclick="edit_brand('+custObj.BrandId+')"><i class="glyphicon glyphicon-pencil"></i></button>'+
           '<button class="btn btn-danger" onclick="delete_brand('+custObj.BrandId+')"><i class="glyphicon glyphicon-remove"></i></button>'+
           '</td>');
           $("#brand_table").append(tr);

         });
       }
       return Promise.resolve(data);
     });
   }
   function add_brand()
   {
     save_method = 'add';
     $('[name="BrandId"]').val(undefined);

     $('#form_validation')[0].reset(); // reset form on modals
     $('#customModal').modal('show'); // show bootstrap modal
   }

   function edit_brand(id)
   {
     save_method = 'update';
     $('#form_validation')[0].reset(); // reset form on modals

     //Ajax Load data from ajax
     $.ajax({
       url : "<?php echo site_url('/api/brand/brand_details/')?>" + id,
       type: "GET",
       dataType: "JSON",
       success: function(data)
       {
           $('[name="BrandId"]').val(data.data.BrandId);
           $('[name="BrandName"]').val(data.data.BrandName);


           $('#customModal').modal('show'); // show bootstrap modal when complete loaded
           $('.modal-title').text('Edit Brand'); // Set title to Bootstrap modal title

       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           alert('Error get data from ajax');
       }
   });
   }



   function save()
   {
     var url;
     if(save_method == 'add')
     {
         url = "<?php echo site_url('/api/brand/save_brand')?>";
     }
     else
     {
       url = "<?php echo site_url('/api/brand/brand_update')?>";
     }

      // ajax adding data to database
         $.ajax({
           url : url,
           type: "POST",
           data: $('#form_validation').serialize(),
           dataType: "JSON",
           success: function(data)
           {
              //if success close modal and reload ajax table
              $('#customModal').modal('hide');
              fetchBrands();
             // location.reload();// for reload a page
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
           }
       });
   }

   function delete_brand(id)
   {
     if(confirm('Are you sure delete this data?'))
     {
       // ajax delete data from database
         $.ajax({
           url : "<?php echo site_url('/api/brand/brand_update')?>/"+id,
           type: "POST",
           data :{
             BrandId : id,
             status : 1
           },
           dataType: "JSON",
           success: function(data)
           {
             fetchBrands();
              // location.reload();
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error deleting data');
           }
       });

     }
   }
   </script>
