<section class="content">
    <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                AGENTS
                            </h2>
                            <button type="button" onclick="add_agent()" class="btn btn-primary m-t-15 waves-effect" style="float:right; margin-top: -22px;">ADD NEW</button>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>Agent ID</th>
                                            <th>Agent Name</th>
                                            <th>Agent Email</th>
                                            <th>Agent Mobile	</th>
                                            <th>Agent GST</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                      <tr>
                                          <th>Agent ID</th>
                                          <th>Agent Name</th>
                                          <th>Agent Email</th>
                                          <th>Agent Mobile	</th>
                                          <th>Agent GST</th>
                                          <th>Action</th>
                                      </tr>
                                    </tfoot>

                                    <tbody id="agent_table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <div class="modal fade" id="customModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Agents</h4>
                </div>
				<form id="form_validation" action="#">
                <div class="modal-body">
					<input type="hidden" value="" name="AgentId"/>
                  <div class="row clearfix">
                                <div class="col-sm-12">
                                  <div class="row">
                                      <div class="col-lg-4 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                          <label for="confirmEmail">Agent Name</label>
                                      </div>
                                      <div class="col-lg-8 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="AgentName" required placeholder="Agent Name">
                                            </div>
                                        </div>
                                      </div>
                                  </div>

                                  <div class="row">
                                      <div class="col-lg-4 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                          <label for="confirmEmail">Agent Email</label>
                                      </div>
                                      <div class="col-lg-8 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name= "Email" required placeholder="Agent Email">
                                            </div>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-lg-4 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="confirmEmail">Agent Mobile</label>
                                    </div>
                                    <div class="col-lg-8 col-md-10 col-sm-8 col-xs-7">
                                      <div class="form-group">
                                          <div class="form-line">
                                              <input type="text" class="form-control" name="MobileNumber" required placeholder="Agent Mobile">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="confirmEmail">GST</label>
                                    </div>
                                    <div class="col-lg-8 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="gst" required placeholder="GST">
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                
							  </div>
                            </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" onclick="saveAgent()">SAVE </button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
				</form>
            </div>
        </div>
    </div>
</section>

 <!-- Jquery DataTable Plugin Js -->
 <script src="<?php echo base_url('plugins/jquery-validation/jquery.validate.js')?>"></script>
 <script src="<?php echo base_url('js/pages/forms/form-validation.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/jquery.dataTables.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/jszip.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/pdfmake.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/vfs_fonts.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.print.min.js')?>"></script>


  <script type="text/javascript">
$(document).ready( function () {
      fetchAgents().then(reloadDataTable);
});

function saveAgent() {
  var valid = $("#form_validation").valid();
  if (valid) {
    save();
  }
}

  function reloadDataTable() {
    setTimeout(function () {
      $('.dataTable').DataTable();
    },100)
  }
    var save_method; //for save method string
    var table;

    function fetchAgents() {
      return $.ajax({
        url : "<?php echo site_url('/api/agent/all_agents/')?>",
        type: "GET",
        dataType: "JSON"
      }).then(function (data) {
        if (data.status) {
          $("#agent_table").empty();
          data.data.forEach((custObj)=>{
            var tr = $('<tr/>');
            tr.append("<td>" + custObj.AgentId + "</td>");
            tr.append("<td>" + custObj.AgentName + "</td>");
            tr.append("<td>" + custObj.Email + "</td>");
            tr.append("<td>" + custObj.MobileNumber + "</td>");
            tr.append("<td>" + custObj.gst  + "</td>");
            tr.append('<td>' +
            '<button class="btn btn-warning" onclick="edit_agent('+custObj.AgentId+')"><i class="glyphicon glyphicon-pencil"></i></button>'+
            '<button class="btn btn-danger" onclick="delete_agent('+custObj.AgentId+')"><i class="glyphicon glyphicon-remove"></i></button>'+
            '</td>');
            $("#agent_table").append(tr);

          });
        }
        return Promise.resolve(data);
      });
    }
    function add_agent()
    {
      save_method = 'add';
      $('[name="AgentId"]').val(undefined);

      $('#form_validation')[0].reset(); // reset form on modals
      $('#customModal').modal('show'); // show bootstrap modal
    }

    function edit_agent(id)
    {
      save_method = 'update';
      $('#form_validation')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('/api/agent/agent_details/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="AgentId"]').val(data.data.AgentId);
            $('[name="AgentName"]').val(data.data.AgentName);
            $('[name="Email"]').val(data.data.Email);
            $('[name="MobileNumber"]').val(data.data.MobileNumber);
            $('[name="gst"]').val(data.data.gst);

            $('#customModal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Agent'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('/api/agent/save_agent')?>";
      }
      else
      {
        url = "<?php echo site_url('/api/agent/agent_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form_validation').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#customModal').modal('hide');
               fetchAgents();
              // location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_agent(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('/api/agent/agent_update')?>/"+id,
            type: "POST",
            data :{
              AgentId : id,
              status : 1
            },
            dataType: "JSON",
            success: function(data)
            {
              fetchAgents();
               // location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }
    </script>
