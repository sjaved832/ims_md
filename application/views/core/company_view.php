<section class="content">
    <div class="container-fluid">
      <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">
                  <div class="header">
                      <h2>Company Details</h2>
                      <ul class="header-dropdown m-r--5">
                          <li class="dropdown">
                              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                  <i class="material-icons">more_vert</i>
                              </a>
                              <ul class="dropdown-menu pull-right">
                                  <li><a href="javascript:void(0);">Cancel</a></li>
                                  <li><a href="javascript:void(0);">Another action</a></li>
                                  <li><a href="javascript:void(0);">Something else here</a></li>
                              </ul>
                          </li>
                      </ul>
                  </div>
                   <div class="body">

                     <form id="company-form" class="form-horizontal company-form"  novalidate>
                        <h3> Company Information</h3>    
										
                       <input type="hidden" id="CompanyId" name="CompanyId" value="">
						
						 <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="CompanyName">Company Name</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="CompanyName" name="CompanyName" class="form-control" placeholder="Enter your Company Name" required>
                                            </div>
                                        </div>
                                    </div>
                         </div>
						
						 <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="Email">Email</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="email" id="Email" name="Email" class="form-control" placeholder="Enter your Email ID" required>
                                            </div>
                                        </div>
                                    </div>
                         </div>
						 
						  <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="confirmEmail">Confirm Email</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="email" id="confirmEmail" name="confirmEmail" class="form-control" placeholder="Please Confirm your Email" required>
                                            </div>
                                        </div>
                                    </div>
                         </div>
						 
						   <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="Mobile">Mobile Number</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" id="Mobile" name="Mobile"  maxlength="12" minlength="10" class="form-control" placeholder="Enter your Mobile Number" required>
                                            </div>
                                        </div>
                                    </div>
                         </div>
						 
						   <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="Website">Website URL</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="url" id="Website" name="Website" class="form-control" placeholder="Enter your Mobile Number" required>
                                            </div>
                                        </div>
                                    </div>
                         </div>
						 
						   <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="ContactName">Contact Name</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="ContactName" name="ContactName" class="form-control" placeholder="Contact Person Name" required>
                                            </div>
                                        </div>
                                    </div>
                         </div>
                         
						
						<div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <button id="send" type="submit" class="btn btn-success m-t-15 waves-effect">Submit</button>
                                        <button type="reset" class="btn btn-primary m-t-15 waves-effect">Cancel</button>
                                    </div>
                        </div>                    
                      
                     </form>
                   </div>
                 </div>
               </div>
             </div>
           </div>
</section>



<script type="text/javascript">

  $(document).ready( function () {
    readCompany();
    $(".btn").click(function (e) {
      e.preventDefault();
      var that = $("#company-form");
      var submit = true;
      // you can put your own custom validations below

      // check all the rerquired fields
	  var submit = $("#company-form").valid();      

      if( submit ){
        let obj = {};
        obj.CompanyId = $("#CompanyId").val();
        obj.CompanyName = $("#CompanyName").val();
        obj.Email = $("#Email").val();
        obj.Mobile = $("#Mobile").val()
        obj.Website = $("#Website").val()
        obj.ContactName = $("#ContactName").val()
        obj.Phone = $("#Phone").val()

        var formData = new FormData();
        for (var index in obj) {
          if (obj.hasOwnProperty(index)) {
            formData.set(index,obj[index]);
          }
        }
        $.ajax({
          url:baseUrl + "/api/company/save_company",
          processData: false,
          contentType: false,
          data : formData,
          method:"POST",
          success : function (res) {
            if (res && res.status) {
              window.location.reload();
            }else {
              alert(res.message)
            }
          }
        })
      }
    });
  })
  function readCompany() {

    $.ajax({
      url: baseUrl + "/api/company/all_companys",
      method : "GET",
      success : function (res) {
        if (res && res.status && res.data && res.data.length > 0) {
          let companyObj = res.data[0];
          $("#CompanyId").val(companyObj.CompanyId);
          $("#CompanyName").val(companyObj.CompanyName);
          $("#Email").val(companyObj.Email);
          $("#confirmEmail").val(companyObj.Email);
          $("#Mobile").val(companyObj.Mobile);
          $("#Website").val(companyObj.Website)
          $("#ContactName").val(companyObj.ContactName)
          $("#Phone").val(companyObj.Phone);
        }
      }
    })
  }
</script>

 <!-- Jquery Core Js -->
    <script src="<?php echo base_url('plugins/jquery/jquery.min.js')?>"></script>

    <!-- Bootstrap Core js')?> -->
    <script src="<?php echo base_url('plugins/bootstrap/js/bootstrap.js')?>"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="<?php echo base_url('plugins/jquery-validation/jquery.validate.js')?>"></script>  

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('plugins/node-waves/waves.js')?>"></script>

    <!-- Custom Js -->

    <script src="<?php echo base_url('js/pages/forms/form-validation.js')?>"></script>

    