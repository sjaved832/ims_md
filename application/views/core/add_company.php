           <div class="right_col" role="main">
             <div class="container">
             <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                 <div class="x_panel">
                   <div class="x_title">
                     <h2>Company <small>information</small></h2>
                     <ul class="nav navbar-right panel_toolbox">
                       <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                       </li>
                       <li><a class="close-link"><i class="fa fa-close"></i></a>
                       </li>
                     </ul>
                     <div class="clearfix"></div>
                   </div>
                   <div class="x_content">

                     <form id="company-form" class="form-horizontal form-label-left company-form" novalidate>
                       </p>
                       <span class="section">Company Details</span>
                       <input type="hidden" name="CompanyId" value="">

                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Company Name <span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input id="CompanyName" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="CompanyName" placeholder="both name(s) e.g Jon Doe Pvt Ltd" required="required" type="text">
                         </div>
                       </div>
                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input type="email" id="Email" name="Email" required="required" class="form-control col-md-7 col-xs-12">
                         </div>
                       </div>
                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Confirm Email <span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input type="email" id="Email2" name="confirmEmail" data-validate-linked="Email" required="required" class="form-control col-md-7 col-xs-12">
                         </div>
                       </div>
                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Mobile Number <span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input type="number" id="Mobile" name="Mobile" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12">
                         </div>
                       </div>
                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="website">Website URL <span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input type="url" id="Website" name="Website" placeholder="www.website.com" class="form-control col-md-7 col-xs-12">
                         </div>
                       </div>
                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="occupation">Contact Person Name <span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input id="ContactName" type="text" name="ContactName" required="required" data-validate-length-range="5,20" class="form-control col-md-7 col-xs-12">
                         </div>
                       </div>
                       <!-- <div class="item form-group">
                         <label for="password" class="control-label col-md-3">Company Logo<span class="required">*</span></label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input accept="image/*"  type="file" name="CompanyLogo"  class="form-control col-md-7 col-xs-12" required="required">
                         </div>
                       </div> -->
                       <div class="item form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">Telephone <span class="required">*</span>
                         </label>
                         <div class="col-md-6 col-sm-6 col-xs-12">
                           <input type="tel" id="Phone" name="Phone" required="required" data-validate-length-range="8,20" class="form-control col-md-7 col-xs-12">
                         </div>
                       </div>
                       <div class="ln_solid"></div>
                       <div class="form-group" style="padding-bottom:21px">
                         <div class="col-md-6 col-md-offset-3">
                           <button type="reset" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">Submit</button>
                         </div>
                       </div>
                     </form>
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </div>

 <script src="<?php echo base_url('assests/jquery/jquery-3.1.0.min.js')?>"></script>
<script type="text/javascript">

  $(document).ready( function () {
    // $('#company-form').submit(function(e){
    $(".btn").click(function (e) {
      e.preventDefault();
      var that = $("#company-form");
      var submit = true;
      // you can put your own custom validations below

      // check all the rerquired fields
      if( !validator.checkAll( $(that) ) )
        submit = false;

      if( submit ){
        let obj = {};
        obj.CompanyName = $("#CompanyName").val();
        obj.Email = $("#Email").val();
        obj.Mobile = $("#Mobile").val()
        obj.Website = $("#Website").val()
        obj.ContactName = $("#ContactName").val()
        obj.Phone = $("#Phone").val()

        var formData = new FormData();
        for (var index in obj) {
          if (obj.hasOwnProperty(index)) {
            formData.set(index,obj[index]);
          }
        }
        // formData.append("CompanyName",$("#CompanyName").val());
        // formData.append("Email",$("#Email").val());
        // formData.append("Mobile",$("#Mobile").val());
        // // formData.append("Website",$("#Website").val());
        // formData.append("ContactName",$("#ContactName").val());
        // formData.append("Phone",$("#Phone").val())
        $.ajax({
          url:"/api/company/save_company",
          processData: false,
          contentType: false,
          data : formData,
          method:"POST",
          success : function (e) {
            // console.log(e);
            window.location = baseUrl;
          }
        })
        // console.log($(that).ajaxSend({url:"/api/company/add_company",method:"POST"}),$(that).ajaxSuccess(()=>{}),[$(that).data(),$(that).val()],$(that).serialize());
      }
        // this.submit();

      // return false;
    });
  })
</script>
