<section class="content">
    <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                CUSTOMERS
                            </h2>
                            <button type="button" onclick="add_customer()" class="btn btn-primary m-t-15 waves-effect" style="float:right; margin-top: -22px;">ADD NEW</button>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>Customer ID</th>
                                            <th>Customer Name</th>
                                            <th>Customer Email</th>
                                            <th>Customer Mobile	</th>
                                            <th>Customer Area</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                      <tr>
                                          <th>Customer ID</th>
                                          <th>Customer Name</th>
                                          <th>Customer Email</th>
                                          <th>Customer Mobile	</th>
                                          <th>Customer Area</th>
                                          <th>Action</th>
                                      </tr>
                                    </tfoot>

                                    <tbody id="customer_table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <div class="modal fade" id="customModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Customers</h4>
                </div>
				<form id="form_validation" action="#">
                <div class="modal-body">
					<input type="hidden" value="" name="CustomerId"/>
                  <div class="row clearfix">
                                <div class="col-sm-12">
                                  <div class="row">
                                      <div class="col-lg-4 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                          <label for="confirmEmail">Customer Name</label>
                                      </div>
                                      <div class="col-lg-8 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="CustomerName" required placeholder="Customer Name">
                                            </div>
                                        </div>
                                      </div>
                                  </div>

                                  <div class="row">
                                      <div class="col-lg-4 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                          <label for="confirmEmail">Customer Email</label>
                                      </div>
                                      <div class="col-lg-8 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name= "Email" required placeholder="Customer Email">
                                            </div>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-lg-4 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="transport">Transport</label>
                                    </div>
                                    <div class="col-lg-8 col-md-10 col-sm-8 col-xs-7">
                                      <div class="form-group">
                                          <div class="form-line">
                                              <select class="form-control" name="transport" id="transport" required>
											  </select>
                                          </div>
                                      </div>
                                    </div>
                                  </div>
								  <div class="row">
                                    <div class="col-lg-4 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="confirmEmail">Customer Mobile</label>
                                    </div>
                                    <div class="col-lg-8 col-md-10 col-sm-8 col-xs-7">
                                      <div class="form-group">
                                          <div class="form-line">
                                              <input type="text" class="form-control" name="MobileNumber" required placeholder="Customer Mobile">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="confirmEmail">Customer Area</label>
                                    </div>
                                    <div class="col-lg-8 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="CustomerArea" required placeholder="Customer Area">
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="confirmEmail">Max Bill Payment Days</label>
                                    </div>
                                    <div class="col-lg-8 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="MaxBillPayDays" required placeholder="Max Bill Pay Days">
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="confirmEmail">Max Bill Payment Amount</label>
                                    </div>
                                    <div class="col-lg-8 col-md-10 col-sm-8 col-xs-7">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="MaxBillPayAmount" required placeholder="Max Bill Pay Amount">
                                        </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" onclick="saveCustomer()">SAVE </button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
				</form>
            </div>
        </div>
    </div>
</section>

 <!-- Jquery DataTable Plugin Js -->
 <script src="<?php echo base_url('plugins/jquery-validation/jquery.validate.js')?>"></script>
 <script src="<?php echo base_url('js/pages/forms/form-validation.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/jquery.dataTables.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/jszip.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/pdfmake.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/vfs_fonts.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.print.min.js')?>"></script>


  <script type="text/javascript">
$(document).ready( function () {
      fetchCustomers().then(reloadDataTable);
	  fetchtransports();
	  //fetchAgents();
});

/*function for fetching transports*/
function fetchtransports() {
     return $.ajax({
       url : "<?php echo site_url('/api/transport/all_transports/')?>",
       type: "GET",
       dataType: "JSON"
     }).then(function (data) {
		 console.log(data);return;
       if (data.status) {
		   $.each(data.data,function(key, value)
		   {
				$("#transport").append('<option value=' + key + '>' + value + '</option>');
		   });
         /*data.data.forEach((custObj)=>{
           var tr = $('<tr/>');
           tr.append("<td>" + custObj.transportId + "</td>");
           tr.append("<td>" + custObj.transportName + "</td>");
           tr.append('<td>' +
           '<button class="btn btn-warning" onclick="edit_transport('+custObj.transportId+')"><i class="glyphicon glyphicon-pencil"></i></button>'+
           '<button class="btn btn-danger" onclick="delete_transport('+custObj.transportId+')"><i class="glyphicon glyphicon-remove"></i></button>'+
           '</td>');
           $("#transport_table").append(tr);

         });*/
       }
       //return Promise.resolve(data);
     });
   }
   
function saveCustomer() {
  var valid = $("#form_validation").valid();
  if (valid) {
    save();
  }
}

  function reloadDataTable() {
    setTimeout(function () {
      $('.dataTable').DataTable();
    },100)
  }
    var save_method; //for save method string
    var table;

    function fetchCustomers() {
      return $.ajax({
        url : "<?php echo site_url('/api/customer/all_customers/')?>",
        type: "GET",
        dataType: "JSON"
      }).then(function (data) {
        if (data.status) {
          $("#customer_table").empty();
          data.data.forEach((custObj)=>{
            var tr = $('<tr/>');
            tr.append("<td>" + custObj.CustomerId + "</td>");
            tr.append("<td>" + custObj.CustomerName + "</td>");
            tr.append("<td>" + custObj.Email + "</td>");
            tr.append("<td>" + custObj.MobileNumber + "</td>");
            tr.append("<td>" + custObj.CustomerArea  + "</td>");
            tr.append('<td>' +
            '<button class="btn btn-warning" onclick="edit_customer('+custObj.CustomerId+')"><i class="glyphicon glyphicon-pencil"></i></button>'+
            '<button class="btn btn-danger" onclick="delete_customer('+custObj.CustomerId+')"><i class="glyphicon glyphicon-remove"></i></button>'+
            '</td>');
            $("#customer_table").append(tr);

          });
        }
        return Promise.resolve(data);
      });
    }
    function add_customer()
    {
      save_method = 'add';
      $('[name="CustomerId"]').val(undefined);

      $('#form_validation')[0].reset(); // reset form on modals
      $('#customModal').modal('show'); // show bootstrap modal
    }

    function edit_customer(id)
    {
      save_method = 'update';
      $('#form_validation')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('/api/customer/customer_details/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="CustomerId"]').val(data.data.CustomerId);
            $('[name="CustomerName"]').val(data.data.CustomerName);
            $('[name="Email"]').val(data.data.Email);
            $('[name="MobileNumber"]').val(data.data.MobileNumber);
            $('[name="CustomerArea"]').val(data.data.CustomerArea);
            $('[name="MaxBillPayAmount"]').val(data.data.MaxBillPayAmount);
            $('[name="MaxBillPayDays"]').val(data.data.MaxBillPayDays);

            $('#customModal').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Customer'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('/api/customer/save_customer')?>";
      }
      else
      {
        url = "<?php echo site_url('/api/customer/customer_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form_validation').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#customModal').modal('hide');
               fetchCustomers();
              // location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_customer(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('/api/customer/customer_update')?>/"+id,
            type: "POST",
            data :{
              CustomerId : id,
              status : 1
            },
            dataType: "JSON",
            success: function(data)
            {
              fetchCustomers();
               // location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }
    </script>
