<style>
.btnh:hover{
  background-color: #286090!important;
}
</style>
<section class="content">


 <div class="right_col">
    <h3>User</h3>
    <br />
    <a class="btn btn-success" href="<?php echo site_url('/auth/create_user')?>"><i class="glyphicon glyphicon-plus"></i> Add User</a>
    <br />
    <br />
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>User ID</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Password</th>

          <th style="width:125px;">Action
          </p></th>
        </tr>
      </thead>
      <tbody id="user_table">
       

      </tbody>

      <tfoot>
        <tr>
         <th>User ID</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Password</th>
        </tr>
      </tfoot>
    </table>

  </div>
  </section>

  <script src="<?php echo base_url('assests/jquery/jquery-3.1.0.min.js')?>"></script>
  <script src="<?php echo base_url('assests/bootstrap/js/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assests/datatables/js/jquery.dataTables.min.js')?>"></script>
  <script src="<?php echo base_url('assests/datatables/js/dataTables.bootstrap.js')?>"></script>


  <script type="text/javascript">
  $(document).ready( function () {
    // Reload Data Table after reload
      fetchCustomers().then(reloadDataTable);

      $("#form").validate({
    		submitHandler:function(form)
    		{
    	     save(form);
           return false;
    		},
    		errorElement: 'span',
    		errorClass: 'help-block error',
    		rules:
    		{
    			CustomerName:"required",
    			CustomerArea:"required"
    		},
    		messages:
    		{	CustomerName:"Please Enter Customer Name",
    			CustomerArea:"Please Enter Customer Area"
    		},
    		ignore: []
    	});
});
function saveCustomer() {
  var valid = $("#form").valid();
  if (valid) {
    save();
  }
}

  function reloadDataTable() {
    setTimeout(function () {
      $('#table_id').DataTable();
    },100)
  }
    var save_method; //for save method string
    var table;

    function fetchCustomers() {
      return $.ajax({
        url : "<?php echo site_url('/api/user/all_users/')?>",
        type: "GET",
        dataType: "JSON"
      }).then(function (data) {
        if (data.status) {
          $("#user_table").empty();
          data.data.forEach((usrObj)=>{
            var tr = $('<tr/>');
            tr.append("<td>" + usrObj.id + "</td>");
            tr.append("<td>" + usrObj.first_name + "</td>");
            tr.append("<td>" + usrObj.last_name + "</td>");
            tr.append("<td>" + usrObj.email + "</td>");
            tr.append("<td>" + usrObj.phone + "</td>");
            tr.append("<td>" + usrObj.password  + "</td>");
            tr.append('<td>' +
            '<button class="btn btn-warning" onclick="edit_user('+usrObj.id+')"><i class="glyphicon glyphicon-pencil"></i></button>'+
            '<button class="btn btn-danger" onclick="delete_user('+usrObj.id+')"><i class="glyphicon glyphicon-remove"></i></button>'+
            '</td>');
            $("#user_table").append(tr);
            // <tr>
            //     <td></td>
            //     <td></td>
            //     <td></td>
            //    <td></td>
            //    <td></td>
            //    <td>
            //      <button class="btn btn-warning" onclick="edit_customer()"><i class="glyphicon glyphicon-pencil"></i></button>
            //      <button class="btn btn-danger" onclick="delete_customer()"><i class="glyphicon glyphicon-remove"></i></button>
            //    </td>
            //  </tr>
          });
        }
        return Promise.resolve(data);
      });
    }
   

    function edit_user(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('/api/user/all_users/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.data.id);
            $('[name="first_name"]').val(data.data.first_name);
            $('[name="last_name"]').val(data.data.last_name);
            $('[name="email"]').val(data.data.email);
            $('[name="phone"]').val(data.data.phone);
            $('[name="password"]').val(data.data.password);

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit User'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('/api/user/create_user')?>";
      }
      else
      {
        url = "<?php echo site_url('/api/user/create_user')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               fetchCustomers();
              // location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_user(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo site_url('/api/user/create_user')?>/"+id,
            type: "POST",
            data :{
              CustomerId : id,
              status : 1
            },
            dataType: "JSON",
            success: function(data)
            {
              fetchCustomers();
               // location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }

  </script>

  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Customer Form</h3>
      </div>
      <form action="#" id="form" class="form-horizontal" >

      <div class="modal-body form">
          <input type="hidden" value="" name="id"/>
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">First Name</label>
              <div class="col-md-9">
                <input name="first_name" placeholder="First Name" required class="form-control" type="text">
              </div>
            </div>
             <div class="form-group">
              <label class="control-label col-md-3">First Name</label>
              <div class="col-md-9">
                <input name="last_name" placeholder="Last Name" required class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Email</label>
              <div class="col-md-9">
                <input name="email" placeholder="Email" type="email" required class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Mobile</label>
              <div class="col-md-9">
                <input name="phone" placeholder="Mobile" type="number" required class="form-control" type="text">

              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Password</label>
              <div class="col-md-9">
                <input name="password" placeholder="Password" required class="form-control" type="text">
              </div>
            </div>

          </div>
          </div>
          <div class="modal-footer">
            <button type="button" onclick="saveCustomer()" id="btnSave" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
