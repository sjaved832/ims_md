<section class="content">
    <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                COLORS
                            </h2>
                            <button type="button" onclick="add_color()" class="btn btn-primary m-t-15 waves-effect" style="float:right; margin-top: -22px;">ADD NEW</button>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>Color ID</th>
                                            <th>Color Name</th>

                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                      <tr>
                                          <th>Color ID</th>
                                          <th>Color Name</th>

                                          <th>Action</th>
                                      </tr>
                                    </tfoot>

                                    <tbody id="color_table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <div class="modal fade" id="customModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Colors</h4>
                </div>
                <form id="form_validation" action="#">
                        <div class="modal-body">
                  <input type="hidden" value="" name="ColorId"/>
                          <div class="row clearfix">
                                        <div class="col-sm-12">

                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="ColorName" required placeholder="Color Name">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-link waves-effect" onclick="saveColor()">SAVE </button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                </form>

            </div>
        </div>
    </div>
</section>

 <!-- Jquery DataTable Plugin Js -->
 <script src="<?php echo base_url('plugins/jquery-datatable/jquery.dataTables.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/jszip.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/pdfmake.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/vfs_fonts.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.print.min.js')?>"></script>


 <script type="text/javascript">
$(document).ready( function () {
     fetchColors().then(reloadDataTable);
});

function saveColor() {
 var valid = $("#form_validation").valid();
 if (valid) {
   save();
 }
}

 function reloadDataTable() {
   setTimeout(function () {
     $('.dataTable').DataTable().draw();
   },100)
 }
 function destoryTable() {
   // $('.dataTable').DataTable().destroy();
 }

   var save_method; //for save method string
   var table;

   function fetchColors() {
     destoryTable();

     return $.ajax({
       url : "<?php echo site_url('/api/color/all_colors/')?>",
       type: "GET",
       dataType: "JSON"
     }).then(function (data) {
       if (data.status) {
         $("#color_table").empty();
         data.data.forEach((custObj)=>{
           var tr = $('<tr/>');
           tr.append("<td>" + custObj.ColorId + "</td>");
           tr.append("<td>" + custObj.ColorName + "</td>");
           tr.append('<td>' +
           '<button class="btn btn-warning" onclick="edit_color('+custObj.ColorId+')"><i class="glyphicon glyphicon-pencil"></i></button>'+
           '<button class="btn btn-danger" onclick="delete_color('+custObj.ColorId+')"><i class="glyphicon glyphicon-remove"></i></button>'+
           '</td>');
           $("#color_table").append(tr);

         });
       }
       return Promise.resolve(data);
     });
   }
   function add_color()
   {
     save_method = 'add';
     $('[name="ColorId"]').val(undefined);

     $('#form_validation')[0].reset(); // reset form on modals
     $('#customModal').modal('show'); // show bootstrap modal
   }

   function edit_color(id)
   {
     save_method = 'update';
     $('#form_validation')[0].reset(); // reset form on modals

     //Ajax Load data from ajax
     $.ajax({
       url : "<?php echo site_url('/api/color/color_details/')?>" + id,
       type: "GET",
       dataType: "JSON",
       success: function(data)
       {
           $('[name="ColorId"]').val(data.data.ColorId);
           $('[name="ColorName"]').val(data.data.ColorName);


           $('#customModal').modal('show'); // show bootstrap modal when complete loaded
           $('.modal-title').text('Edit Color'); // Set title to Bootstrap modal title

       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           alert('Error get data from ajax');
       }
   });
   }



   function save()
   {
     var url;
     if(save_method == 'add')
     {
         url = "<?php echo site_url('/api/color/save_color')?>";
     }
     else
     {
       url = "<?php echo site_url('/api/color/color_update')?>";
     }

      // ajax adding data to database
         $.ajax({
           url : url,
           type: "POST",
           data: $('#form_validation').serialize(),
           dataType: "JSON",
           success: function(data)
           {
              //if success close modal and reload ajax table
              $('#customModal').modal('hide');
              fetchColors().then(reloadDataTable);
              // location.reload();// for reload a page
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
           }
       });
   }

   function delete_color(id)
   {
     if(confirm('Are you sure delete this data?'))
     {
       // ajax delete data from database
         $.ajax({
           url : "<?php echo site_url('/api/color/color_update')?>/"+id,
           type: "POST",
           data :{
             ColorId : id,
             status : 1
           },
           dataType: "JSON",
           success: function(data)
           {
                fetchColors().then(reloadDataTable);
              // location.reload();
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error deleting data');
           }
       });

     }
   }
   </script>
