<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>INVENTORY SYSTEM</title>
    <!-- <title>Welcome To | Bootstrap Based Admin Template - Material Design</title> -->
    <!-- Favicon-->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!-- <link rel="shortcut icon" type="image/png" href="/favicon.png"/> -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('plugins/bootstrap/css/bootstrap.css')?>" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('plugins/node-waves/waves.css')?>" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url('plugins/animate-css/animate.css')?>" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="<?php echo base_url('plugins/morrisjs/morris.css')?>" rel="stylesheet" />
  <link href="<?php echo base_url('plugins/bootstrap-select/css/bootstrap-select.css')?>" rel="stylesheet" />
    <!-- JQuery DataTable Css -->
    <link href="<?=base_url('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')?>" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?php echo base_url('css/style.css')?>" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url('css/themes/all-themes.css')?>" rel="stylesheet" />
    <link href="<?php echo base_url('plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('plugins/bootstrap-select/css/bootstrap-select.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('plugins/nouislider/nouislider.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css')?>" rel="stylesheet" />
    <link href="<?php echo base_url('plugins/jquery-spinner/css/bootstrap-spinner.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('plugins/multi-select/css/multi-select.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('plugins/dropzone/dropzone.css')?>" rel="stylesheet">


    <!-- Jquery Core Js -->
    <script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>





    <!-- Jquery Validation Js -->
    <script src="<?=base_url('plugins/jquery-validation/jquery.validate.js')?>"></script>

    <script type="text/javascript">
      var baseUrl = '<?=base_url()?>';
    </script>
</head>

<body class="theme-red">

    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
          <!-- remove -->
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?=base_url("/")?>"><?=$company_data['CompanyName']?></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count">7</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">NOTIFICATIONS</li>
                            <li class="body">
                                <ul class="menu">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">person_add</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>12 new members joined</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 14 mins ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-cyan">
                                                <i class="material-icons">add_shopping_cart</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>4 sales made</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 22 mins ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">delete_forever</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>Nancy Doe</b> deleted account</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 3 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-orange">
                                                <i class="material-icons">mode_edit</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>Nancy</b> changed name</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 2 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-blue-grey">
                                                <i class="material-icons">comment</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>John</b> commented your post</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 4 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">cached</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>John</b> updated status</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 3 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-purple">
                                                <i class="material-icons">settings</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Settings updated</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> Yesterday
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">View All Notifications</a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Notifications -->
                    <!-- Tasks -->
                      <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">flag</i>
                            <span class="label-count">9</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">TASKS</li>
                            <li class="body">
                                <ul class="menu tasks">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Footer display issue
                                                <small>32%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 32%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Make new buttons
                                                <small>45%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Create new dashboard
                                                <small>54%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 54%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Solve transition issue
                                                <small>65%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Answer GitHub questions
                                                <small>92%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 92%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">View All Tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Tasks -->
                </ul>
            </div>

          <!-- Remove -->

                      <!-- Company - Developed by <a href="https://growingbiz.in">Growingbiz</a> -->

                  </div>
              </nav>
              <!-- #Top Bar -->
              <section>
                  <!-- Left Sidebar -->
                  <aside id="leftsidebar" class="sidebar">
                      <!-- User Info -->
                      <div class="user-info">
                          <div class="image">
                              <img src="<?=base_url('images/user.png');?>" width="48" height="48" alt="User" />
                          </div>
                          <div class="info-container">
                              <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $user_data->first_name;?></div>
                              <div class="email"><?php echo $user_data->email;?></div>
                              <div class="btn-group user-helper-dropdown">
                                  <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                                  <ul class="dropdown-menu pull-right">
                                      <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                                      <li role="seperator" class="divider"></li>
                                      <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                                      <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                                      <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                                      <li role="seperator" class="divider"></li>
                                      <li><a href="<?=base_url('auth/logout')?>"><i class="material-icons">input</i>Sign Out</a></li>
                                  </ul>
                              </div>
                          </div>
                      </div>
                      <!-- #User Info -->
                      <!-- Menu -->
                      <div class="menu">
                          <ul class="list">
                              <li class="header">MAIN NAVIGATION</li>
                              <li class="active">
                                  <a href="<?=base_url('dashboard');?>">
                                      <i class="material-icons">dashboard</i>
                                      <span>Dashboard</span>
                                  </a>
                              </li>
                              <li>
                                  <a href="javascript:void(0);" class="menu-toggle">
                                      <i class="material-icons">note_add</i>
                                      <span>Masters</span>
                                  </a>
                                  <ul class="ml-menu">
                                      <li>
                                          <a href="<?=base_url('customer');?>">Customers</a>
                                      </li>
									  <li>
                                          <a href="<?=base_url('agent');?>">Agents</a>
                                      </li>
                                      <li>
                                          <a href="<?=base_url('brand');?>">Brands</a>
                                      </li>
									  <li>
                                          <a href="<?=base_url('transport');?>">Transport</a>
                                      </li>
                                      <li>
                                          <a href="<?=base_url('color');?>">Colors</a>
                                      </li>
                                      <li>
                                          <a href="<?=base_url('product');?>">Products</a>
                                      </li>
                                      <li>
                                          <a href="<?=base_url('company');?>">Company</a>
                                      </li>
                                      <li>
                                          <a href="<?=base_url('user');?>">Users</a>
                                      </li>

                                  </ul>
                              </li>
                              <li>
                                  <a href="javascript:void(0);" class="menu-toggle">
                                      <i class="material-icons">store</i>
                                      <span>Stock</span>
                                  </a>
                                  <ul class="ml-menu">
                                      <li>
                                          <a href="<?=base_url('livestock');?>">Live</a>
                                      </li>
                                      <li>
                                          <a href="<?=base_url('returnstock');?>">Returns</a>
                                      </li>


                                  </ul>
                              </li>
                              <li>
                                  <a href="javascript:void(0);" class="menu-toggle">
                                      <i class="material-icons">shop</i>
                                      <span>Sales</span>
                                  </a>
                                  <ul class="ml-menu">
                                      <li>
                                          <a href="<?=base_url('salesorder');?>">Sales Order</a>
                                      </li>
                                      <li>
                                          <a href="<?=base_url('salesreturns');?>">Sales Returns</a>
                                      </li>
                                      <li>
                                          <a href="<?=base_url('invoice');?>">Invoices</a>
                                      </li>
                                      <li>
                                          <a href="<?=base_url('deliverychallan');?>">Delivery Challan</a>
                                      </li>
                                      <!-- <li>
                                          <a href="<?=base_url('');?>">Returns</a>
                                      </li> -->
                                  </ul>
                              </li>

                              <li>
                                  <a href="javascript:void(0);" class="menu-toggle">
                                      <i class="material-icons">add</i>
                                      <span>Purchase</span>
                                  </a>
                                  <ul class="ml-menu">
                                      <li>
                                          <a href="<?=base_url('');?>">Purchase</a>
                                      </li>
                                      <li>
                                          <a href="<?=base_url('');?>">Returns</a>
                                      </li>


                                  </ul>
                              </li>
                              <li>
                                  <a href="pages/typography.html">
                                      <i class="material-icons">assessment</i>
                                      <span>Reports</span>
                                  </a>
                              </li>




                          </ul>
                      </div>
                      <!-- #Menu -->
                      <!-- Footer -->
                      <div class="legal">
                          <div class="copyright">
                              &copy; 2018 <a href="javascript:void(0);">GrowingBiz</a>.
                          </div>

                      </div>
                      <!-- #Footer -->
                  </aside>
                  <!-- #END# Left Sidebar -->
              </section>
