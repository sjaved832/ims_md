


    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('plugins/bootstrap/js/bootstrap.js')?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('plugins/bootstrap-select/js/bootstrap-select.js')?>"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url('plugins/jquery-slimscroll/jquery.slimscroll.js')?>"></script>
    <script src="<?php echo base_url('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js')?>"></script>
    <script src="<?php echo base_url('plugins/dropzone/dropzone.js')?>"></script>
    <script src="<?php echo base_url('plugins/jquery-inputmask/jquery.inputmask.bundle.js')?>"></script>
    <script src="<?php echo base_url('plugins/jquery-spinner/js/jquery.spinner.js')?>"></script>
    <script src="<?php echo base_url('plugins/nouislider/nouislider.js')?>"></script>
    <script src="<?php echo base_url('plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('plugins/node-waves/waves.js')?>"></script>
    <script src="<?php echo base_url('plugins/multi-select/js/jquery.multi-select.js')?>"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<?php echo base_url('plugins/jquery-countto/jquery.countTo.js')?>"></script>
    <!-- Custom Js -->
    <script src="<?php echo base_url('js/admin.js')?>"></script>
    <!-- Demo Js -->
    <script src="<?php echo base_url('js/demo.js')?>"></script>





</body>

</html>
