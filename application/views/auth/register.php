
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">    
	<title>Register | Inventory Management System - GrowingBiz</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url();?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url();?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url();?>plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet">
</head>

<body class="signup-page">
    <div class="signup-box">
        <div class="logo">
             <a href="javascript:void(0);">Growing<b>BIZ</b></a>
            <small>Login - Inventory Management System </small>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_up" method="POST" action = "<?php echo base_url('auth/create_user')?>">
                    <div class="msg">Register a new user</div>
						 <div id="infoMessage"><?php echo $message;?></div>
						<div class="input-group">
							<span class="input-group-addon">
								<i class="material-icons">person</i>
							</span>
							 
									 <div class="form-line">      
										<?php echo form_input($first_name);?>									 
										<!--<input type="text" class="form-control" name="first_name" placeholder="First Name" required autofocus>                   -->
									</div>								 					
							 		 <div class="form-line">       
										 <?php echo form_input($last_name);?>
											<!--<input type="text" class="form-control" name="last_name" placeholder="Last Name" required>-->
										</div>
									
						</div>				
						<?php
						  if($identity_column!=='email') {
							  echo '<p>';
							  echo lang('create_user_identity_label', 'identity');
							  echo '<br />';
							  echo form_error('identity');
							  echo form_input($identity);
							  echo '</p>';
						  }
						 ?>						
						<div class="input-group">
							<span class="input-group-addon">
								<i class="material-icons">email</i>
							</span>
							<div class="form-line">
							<?php echo form_input($email);?>
								<!--<input type="email" class="form-control" name="email" placeholder="Email Address" required>-->
							</div>
						</div>
					  <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">phone</i>
                        </span>
                        <div class="form-line">
						 <?php echo form_input($phone);?>
                           <!-- <input type="number" class="form-control" name="phone" placeholder="Phone Number" required>-->
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
						<?php echo form_input($password);?>
                           <!-- <input type="password" class="form-control" name="password" minlength="6" placeholder="Password" required>-->
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
						<?php echo form_input($password_confirm);?>
                           <!-- <input type="password" class="form-control" name="confirm" minlength="6" placeholder="Confirm Password" required>-->
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink">
                        <label for="terms">I read and agree to the <a href="javascript:void(0);">terms of usage</a>.</label>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">SIGN UP</button>

                    <div class="m-t-25 m-b--5 align-center">
                        <a href="<?php echo base_url('auth/login')?>">Login here</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url();?>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url();?>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url();?>plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url();?>plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url();?>js/admin.js"></script>
    <script src="<?php echo base_url();?>js/pages/examples/sign-up.js"></script>
</body>

</html>


