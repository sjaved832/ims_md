<link href="<?php echo base_url('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')?>" rel="stylesheet">


<section class="content">
    <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Deliverychallans
                            </h2>

                                <a href="<?=base_url('deliverychallan/add_deliverychallan')?>" type="button" class="btn btn-primary m-t-15 waves-effect " style="float:right; margin-top: -22px;">ADD NEW</a>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                          <th>Deliverychallan ID</th>
                                          <th>Deliverychallan Name</th>
                                          <th>Total Payment</th>
                                          <th>Created Date</th>

                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                      <tr>
                                        <th>Deliverychallan ID</th>
                                        <th>Deliverychallan Name</th>
                                        <th>Total Payment</th>
                                        <th>Created Date</th>

                                        <th>Action</th>
                                      </tr>
                                    </tfoot>

                                    <tbody id="deliverychallan_table">
                                      <td>I0001</td>
                                      <td>Ashton Cox</td>
                                      <td>660</td>
                                      <td>2009/01/12</td>
                                      <td><a href="<?=base_url('deliverychallan/deliverychallan_view')?>" type="button" class="btn btn-primary m-t-15 waves-effect">View</a></td>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>


</section>

 <!-- Jquery DataTable Plugin Js -->
 <script src="<?php echo base_url('plugins/jquery-datatable/jquery.dataTables.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/jszip.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/pdfmake.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/vfs_fonts.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.print.min.js')?>"></script>


 <script type="text/javascript">
$(document).ready( function () {
     fetchDeliverychallan().then(reloadDataTable);
});

function saveDeliverychallan() {
 var valid = $("#form_validation").valid();
 if (valid) {
   save();
 }
}

 function reloadDataTable() {
   setTimeout(function () {
     $('.dataTable').DataTable();
   },100)
 }
   var save_method; //for save method string
   var table;

   function fetchDeliverychallan() {
     return $.ajax({
       url : "<?php echo site_url('/api/deliverychallan/all_deliverychallan/')?>",
       type: "GET",
       dataType: "JSON"
     }).then(function (data) {
       if (data.status) {
         $("#deliverychallan_table").empty();
         data.data.forEach((custObj)=>{
           var tr = $('<tr/>');
           tr.append("<td>" + custObj.DeliverychallanId + "</td>");
           tr.append("<td>" + custObj.CustomerId + "</td>");
           tr.append("<td>" + custObj.CreatedDate + "</td>");
           tr.append("<td>" + custObj.Amount + "</td>");
           tr.append("<td>" + custObj.TaxAmout + "</td>");
           tr.append("<td>" + custObj.TotalAmount + "</td>");
                       tr.append('<td>' +
           '<button class="btn btn-warning" onclick="edit_deliverychallan('+custObj.DeliverychallanId+')"><i class="glyphicon glyphicon-pencil"></i></button>'+
           '<button class="btn btn-danger" onclick="delete_deliverychallan('+custObj.DeliverychallanId+')"><i class="glyphicon glyphicon-remove"></i></button>'+
           '</td>');
           $("#deliverychallan_table").append(tr);

         });
       }
       return Promise.resolve(data);
     });
   }




   function delete_deliverychallan(id)
   {
     if(confirm('Are you sure delete this data?'))
     {
       // ajax delete data from database
         $.ajax({
           url : "<?php echo site_url('/api/deliverychallan/deliverychallan_update')?>/"+id,
           type: "POST",
           data :{
             DeliverychallanId : id,
             status : 1
           },
           dataType: "JSON",
           success: function(data)
           {
             fetchDeliverychallans();
              // location.reload();
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error deleting data');
           }
       });

     }
   }
   </script>
