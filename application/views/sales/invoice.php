


<style>
.open > .dropdown-menu {
    display: block;
    overflow: initial !important;
}
.bootstrap-select{
  padding: 2px !important;
}
</style>
<style>
.invoice-box {
    max-width: 800px;
    margin: auto;
    padding: 30px;
    border: 1px solid #eee;
    box-shadow: 0 0 10px rgba(0, 0, 0, .15);
    font-size: 16px;
    line-height: 24px;
    font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    color: #555;
}

.invoice-box table {
    width: 100%;
    line-height: inherit;
    text-align: left;
}

.invoice-box table td {
    padding: 5px;
    vertical-align: top;
}

.invoice-box table tr td:nth-child(2) {
    text-align: right;
}

.invoice-box table tr.top table td {
    padding-bottom: 20px;
}

.invoice-box table tr.top table td.title {
    font-size: 45px;
    line-height: 45px;
    color: #333;
}

.invoice-box table tr.information table td {
    padding-bottom: 40px;
}

.invoice-box table tr.heading td {
    background: #eee;
    border-bottom: 1px solid #ddd;
    font-weight: bold;
}

.invoice-box table tr.details td {
    padding-bottom: 20px;
}

.invoice-box table tr.item td{
    border-bottom: 1px solid #eee;
}

.invoice-box table tr.item.last td {
    border-bottom: none;
}

.invoice-box table tr.total td:nth-child(2) {
    border-top: 2px solid #eee;
    font-weight: bold;
}

@media only screen and (max-width: 600px) {
    .invoice-box table tr.top table td {
        width: 100%;
        display: block;
        text-align: center;
    }

    .invoice-box table tr.information table td {
        width: 100%;
        display: block;
        text-align: center;
    }
}

/** RTL **/
.rtl {
    direction: rtl;
    font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
}

.rtl table {
    text-align: right;
}

.rtl table tr td:nth-child(2) {
    text-align: left;
}
</style>
<section class="content">
    <div class="container-fluid">
      <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">
                  <div class="header">
                      <h2>Invoices</h2>
                      <ul class="header-dropdown m-r--5">
                          <li class="dropdown">
                              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                  <i class="material-icons">more_vert</i>
                              </a>
                              <ul class="dropdown-menu pull-right">
                                  <li><a href="javascript:void(0);" onclick="gotoProduct();">Cancel</a></li>
                                  <li><a href="javascript:void(0);">Another action</a></li>
                                  <li><a href="javascript:void(0);">Something else here</a></li>
                              </ul>
                          </li>
                      </ul>
                  </div>
                  <div class="body">
                    <div class="x_content" id="popup-wrapper">

                        <!-- title row -->

                        <!-- info row -->
                        <div id="popup-wrapper">
                        <div class="invoice-box" style="background-color: #ffffff;">
                            <table cellpadding="0" cellspacing="0">
                                <tr class="top">
                                    <td colspan="6">
                                        <table>
                                            <tr>
                                                <td class="title">
                                                <h5 style="margin:0px; float:center;">  بسم اللہ تاجروں </h5>Bismillah Traders  <!--img src="https://www.sparksuite.com/images/logo.png" style="width:100%; max-width:300px;"-->
                                                </td>

                                                <td>
                                                    Invoice #: 123<br>
                                                    Created: January 1, 2015<br>
                                                    Due: February 1, 2015
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="information">
                                    <td colspan="6">
                                        <table>
                                            <tr>
                                                <td>
                                                    Plot no. 9/J/1,Road no.8,<br>
                                                  Baiganwadi,Shivaji Nagar,<br>
                                                    Govandi,Mumbai-400 043
                                                </td>

                                                <td>
                                                    Acme Corp.<br>
                                                    John Doe<br>
                                                    john@example.com
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="heading">
                                    <td colspan="5">
                                        Payment Method
                                    </td>

                                    <td>
                                        Check #
                                    </td>
                                </tr>

                                <tr class="details">
                                    <td colspan="5">
                                        Check
                                    </td>

                                    <td>
                                        1000
                                    </td>
                                </tr>

                                <tr class="heading">
                                    <td>
                                        Item no.
                                    </td>
                                    <td> Description</td>
                                    <td>HSN code.</td>
                                    <td>Quantity</td>
                                    <td>Rate</td>
                                    <td>
                                        Price
                                    </td>
                                </tr>

                                <tr class="item">
                                    <td>

                                    </td>
                                    <td>Website design</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        $300.00
                                    </td>
                                </tr>

                                <tr class="item">
                                  <td></td>

                                    <td>
                                        Hosting (3 months)
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td>
                                        $75.00
                                    </td>
                                </tr>

                                <tr class="item last">
                                  <td></td>

                                    <td>
                                        Domain name (1 year)
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td text-align="right">
                                        $10.00
                                    </td>
                                </tr>

                                <tr class="total">
                                    <td colspan="5" ></td>



                                    <td>
                                       Total: $385.00
                                    </td>
                                </tr>
                                <tr class="information">
                                    <td colspan="6">
                                        <table>
                                            <tr>
                                                <td>
                                                  <h4>Terms & Conditions</h4>
                                                    Plot no. 9/J/1,Road no.8,<br>
                                                  Baiganwadi,Shivaji Nagar,<br>
                                                    Govandi,Mumbai-400 043
                                                </td>

                                                <td>
                                                  <H5>GST IN- 0005416488549</h5>
                                                    <H5>PAN NO- 0005416488549</h5>
                                                    John Doe<br>
                                                    john@example.com
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="information">
                                    <td colspan="6">
                                        <table>
                                            <tr>
                                                <td>
                                                  <h4 style="margin:0;">ThankYOU</h4>
                                                  Visit again
                                                    </td>

                                                <td>
                                                  For
                                                    <H5 style="margin:0;">Bismillah Traders</h5>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>

                        </div>
                  </div>
                        <!-- /.row -->

                        <!-- this row will not appear when printing -->
                        <div class="row no-print">
                          <div class="col-xs-12">
                            <button class="btn btn-default" onclick="PrintMe('popup-wrapper')"><i class="fa fa-print"></i> Print</button>
                            <button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment</button>
                            <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                          </div>
                        </div>
                    </div>
                  </div>
              </div>
            </div>
      </div>

    </div>
</section>

<!-- JQuery Steps Plugin Js -->
<script src="<?=base_url('plugins/jquery-steps/jquery.steps.js')?>"></script>

<!-- Sweet Alert Plugin Js -->
<script src="<?=base_url('plugins/sweetalert/sweetalert.min.js')?>"></script>
<script>
            function PrintMe(DivID) {
            var disp_setting="toolbar=yes,location=no,";
            disp_setting+="directories=yes,menubar=yes,";
            disp_setting+="scrollbars=no,width=800px,height=1000px";
               var content_vlue = document.getElementById(DivID).innerHTML;
               var docprint=window.open("","",disp_setting);
               docprint.document.open();
               docprint.document.write('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"');
               docprint.document.write('"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">');
               docprint.document.write('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">');
               docprint.document.write('<head><title>GrowingBiz :: Sales Invoice</title>');
               docprint.document.write('<link href="<?php echo base_url('css/hami.css')?>" rel="stylesheet">');



               //docprint.document.write('<style type="text/css">body{ margin:0px 0px 0px 0px;)</style>');
               docprint.document.write('</head><body onLoad="self.print()" style="background-color: #fff;border: 1px solid #868686;"');
               docprint.document.write(content_vlue);
               docprint.document.write('</body></html>');
               docprint.document.close();
               docprint.focus();
            }
            </script>
