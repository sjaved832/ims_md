<section class="content">
    <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Sales
                            </h2>
                            <a href="<?=base_url('salesorder/add_salesorder')?>" type="button" class="btn btn-primary m-t-15 waves-effect " style="float:right; margin-top: -22px;">ADD NEW</a>
                            <button id="generateInvoiceButton" class="btn btn-primary m-t-15 waves-effect" style="display:none; float:right; margin-top:-22px; margin-right:10px;"> Add to Invoice </button>

                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                        <td class="">
                           </td>
                                            <th>Sale ID</th>
                                            <th>Customer Name</th>
                                            <th>Sale Date</th>
                                            <th>Discount	</th>
                                            <!-- <th>Tax Amount</th> -->
                                            <th>Net Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                      <tr>
                                          <td class=" "></td>
                                          <th>Sale ID</th>
                                          <th>Customer Name</th>
                                          <th>Sale Date</th>
                                          <th>Discount	</th>
                                          <!-- <th>Tax Amount</th> -->
                                          <th>Net Amount</th>
                                          <th>Action</th>
                                      </tr>
                                    </tfoot>


                                    <tbody id="sales_table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <div class="modal fade" id="saleOrderModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Sales Order</h4>
                </div>
                  <div class="modal-body">

                        <table class="table table-bordered table-striped table-hover dataTable">
                            <thead>
                                <tr>
                                    <th>Sale ID</th>
                                    <th>Customer Name</th>
                                    <th>Sale Date</th>
                                    <th>Discount	</th>
                                    <th>Net Amount</th>
                                </tr>
                            </thead>
                            <tfoot>
                              <tr>
                                  <th>Sale ID</th>
                                  <th>Customer Name</th>
                                  <th>Sale Date</th>
                                  <th>Discount	</th>
                                  <th>Net Amount</th>
                              </tr>
                            </tfoot>


                            <tbody id="sales_confirm_table">
                            </tbody>
                        </table>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-link waves-effect" onclick="generate_invoice_sales()">Confirm </button>
                      <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                  </div>
            </div>
        </div>
    </div>
</section>

 <!-- Jquery DataTable Plugin Js -->
 <script src="<?php echo base_url('plugins/jquery-validation/jquery.validate.js')?>"></script>
 <script src="<?php echo base_url('js/pages/forms/form-validation.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/jquery.dataTables.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/jszip.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/pdfmake.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/vfs_fonts.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.print.min.js')?>"></script>

<script type="text/javascript">
var SelectedSalesId = [];
   $(document).ready( function () {
     // Reload Data Table after reload
       fetchSale().then(reloadDataTable);
 });

 $(document).on("change",".SalesID",function () {
   SelectedSalesId = [];
   $(".SalesID").each(function() {
     if (this.checked) {
       SelectedSalesId.push($(this).attr("data"))
     }
   });
   var button = document.getElementById("generateInvoiceButton");
   if (SelectedSalesId.length > 0){
       button.style.display = "block";
   } else {
      button.style.display = "none";
   }
 });
 $(document).on("click","#generateInvoiceButton",function () {
   $('#saleOrderModal').modal('show'); // show bootstrap modal
   $.ajax({
       url : "<?php echo site_url('/api/sale/fetch_sales/')?>",
       type: "POST",
       dataType: "JSON",
       data : { SelectedSalesId }
   }).then(function (data) {
     if (data.status) {
       $("#sales_confirm_table").empty();
       data.data.forEach((custObj)=>{
         var tr = $('<tr/>');
         tr.append("<td>" + custObj.SalesOrderId + "</td>");
         tr.append("<td>" + custObj.CustomerName + "</td>");
         tr.append("<td>" + custObj.CreatedDate + "</td>");
         tr.append("<td>" + custObj.Discount + "</td>");
         tr.append("<td>" + custObj.TotalPay + "</td>");
         $("#sales_confirm_table").append(tr);
       });
     }
   })
 });
 function generate_invoice_sales() {
   // generate_invoice_sales
   $.ajax({
       url : "<?php echo site_url('/api/sale/generate_invoice_sales/')?>",
       type: "POST",
       dataType: "JSON",
       data : { SelectedSalesId }
   }).then(function (res) {
     $('#saleOrderModal').modal('hide');
     location.reload();
   })
 }

   function reloadDataTable() {
     setTimeout(function () {
       $('.dataTable').DataTable();
     },100)
   }


     function fetchSale() {
       return $.ajax({
         url : "<?php echo site_url('/api/sale/all_sales/')?>",
         type: "GET",
         dataType: "JSON"
       }).then(function (data) {
         if (data.status) {
           $("#sales_table").empty();
           data.data.forEach((custObj)=>{
             var tr = $('<tr/>');
             tr.append("<td>" + '<input type="checkbox" id="myCheck" class="SalesID" data="'+custObj.SalesOrderId+'">' + "</td>")
             tr.append("<td>" + custObj.SalesOrderId + "</td>");
             tr.append("<td>" + custObj.CustomerName + "</td>");
             tr.append("<td>" + custObj.CreatedDate + "</td>");
             tr.append("<td>" + custObj.Discount + "</td>");
             // tr.append("<td>" + custObj.SaleID + "</td>");
             tr.append("<td>" + custObj.TotalPay + "</td>");

             tr.append('<td>' +
             '<button class="btn btn-warning" onclick="edit_sales('+custObj.SalesOrderId+')"><i class="glyphicon glyphicon-pencil"></i></button>'+
             '<button class="btn btn-danger" onclick="delete_sales('+custObj.SalesOrderId+')"><i class="glyphicon glyphicon-remove"></i></button>'+
             '</td>');
             $("#sales_table").append(tr);
           });
         }
         return Promise.resolve(data);
       });
     }


     function edit_sales(id){
      window.location.href = baseUrl + "salesorder/edit_salesorder/"+id;

     }

     function delete_sales(id)
     {
       if(confirm('Are you sure delete this data?'))
       {
         // ajax delete data from database
           $.ajax({
             url : "<?php echo site_url('/api/sale/sales_update')?>/"+id,
             type: "POST",
             data :{
               SalesOrderId : id,
               status : 1
             },
             dataType: "JSON",
             success: function(data)
             {
               fetchSale();
                // location.reload();
             },
             error: function (jqXHR, textStatus, errorThrown)
             {
                 alert('Error deleting data');
             }
         });

       }
     }
     </script>
