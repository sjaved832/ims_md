

<section class="content">
    <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Invoices
                            </h2>

                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                          <th>Invoice ID</th>
                                          <th>Invoice Name</th>
                                          <th>Total Payment</th>
                                          <th>Balance Payment</th>
                                          <th>Created Date</th>

                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                      <tr>
                                        <th>Invoice ID</th>
                                        <th>Invoice Name</th>
                                        <th>Total Payment</th>
                                        <th>Balance Payment</th>
                                        <th>Created Date</th>

                                        <th>Action</th>
                                      </tr>
                                    </tfoot>

                                    <tbody id="invoice_table"/>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>


</section>

 <!-- Jquery DataTable Plugin Js -->
 <script src="<?php echo base_url('plugins/jquery-datatable/jquery.dataTables.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/jszip.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/pdfmake.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/vfs_fonts.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.print.min.js')?>"></script>


   <script type="text/javascript">
   $(document).ready( function () {
     // Reload Data Table after reload
       fetchInvoices().then(reloadDataTable);
    });

   function reloadDataTable() {
     setTimeout(function () {
       $('.dataTable').DataTable();
     },100)
   }


     function fetchInvoices() {
       return $.ajax({
         url : "<?php echo site_url('/api/sale/all_invoice_sales/')?>",
         type: "GET",
         dataType: "JSON"
       }).then(function (data) {
         if (data.status) {
           $("#invoice_table").empty();
           data.data.forEach((custObj)=>{
             var tr = $('<tr/>');
             tr.append("<td>" + custObj.SalesOrderId + "</td>");
             tr.append("<td>" + custObj.CustomerName + "</td>");
             tr.append("<td>" + custObj.TotalPay + "</td>");
             tr.append("<td>" + 0 + "</td>"); //custObj.BalancePay ||
             tr.append("<td>" + custObj.CreatedDate + "</td>");

             tr.append('<td>' +
             '<button class="btn btn-warning" onclick="edit_invoice('+custObj.SalesOrderId+')"><i class="glyphicon glyphicon-pencil"></i></button>'+
             '&nbsp;<button class="btn btn-primary" onclick="edit_invoice('+custObj.SalesOrderId+',1)"><i class="glyphicon glyphicon-folder-open"></i></button>'+
             '</td>');
             $("#invoice_table").append(tr);
           });
         }
         return Promise.resolve(data);
       });
     }

     function edit_invoice(id,view) {
       let action = !view ? "edit_invoice" : "view_invoice";
       window.location.href = baseUrl + "invoice/" + action + "/"+id;
     }
</script>
