<section class="content">
    <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                STOCK
                            </h2>
                            <button type="button" onclick="add_stock()" class="btn btn-primary m-t-15 waves-effect" style="float:right; margin-top: -22px;">ADD NEW</button>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>Stock ID</th>
                                            <th>Product Name</th>
                                            <th>Product Size</th>
                                            <th>Stock Quantity</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                      <tr>
                                          <th>Stock ID</th>
                                          <th>Product Name</th>
                                          <th>Product Size</th>
                                          <th>Stock Quantity</th>
                                          <th>Action</th>
                                      </tr>
                                    </tfoot>

                                    <tbody id="stock_table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <div class="modal fade" id="customModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Stocks</h4>
                </div>
                <form id="form_validation" action="#">
                        <div class="modal-body">
                          <input type="hidden" value="" name="StockId"/>
                          <div class="row clearfix">
                                        <div class="col-sm-12">
                                          <div class="">
                                              <p>
                                                  <b>Product Name </b>
                                              </p>
                                              <select required id="ProductId" name="ProductId" class="form-control">
                                                  <option>---- Please Select ----</option>
                                              </select>
                                          </div>

                                          <div class="">
                                              <p>
                                                  <b>Product Color </b>
                                              </p>
                                              <select required id="ColorId" name="ColorId" class="form-control">
                                                  <option>---- Please Select Color ----</option>
                                              </select>
                                          </div>

                                          <div class="">
                                              <p>
                                                  <b>Product Size </b>
                                              </p>
                                              <select required id="SizeId" name="SizeId" class="form-control">
                                                  <option>---- Please Select Size ----</option>
                                              </select>
                                          </div>

                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" id="Quantity" name="Quantity" required placeholder="Stock Quantity">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-link waves-effect" onclick="saveStock()">SAVE </button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                </form>

            </div>
        </div>
    </div>
</section>

 <!-- Jquery DataTable Plugin Js -->
 <script src="<?php echo base_url('plugins/jquery-datatable/jquery.dataTables.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/jszip.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/pdfmake.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/vfs_fonts.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')?>"></script>
 <script src="<?php echo base_url('plugins/jquery-datatable/extensions/export/buttons.print.min.js')?>"></script>


 <script type="text/javascript">
$(document).ready( function () {
     fetchStocks().then(reloadDataTable);
     fetchProduct()
});

$(document).on("change","#ProductId",function (e) {
  if (e.target.value) {
    fetchProductColor(e.target.value);
	  fetchProductSize(e.target.value);
  }
})

function saveStock() {
 var valid = $("#form_validation").valid();
 if (valid) {
   save();
 }
}

 function reloadDataTable() {
   setTimeout(function () {
     $('.dataTable').DataTable();
   },100)
 }
   var save_method; //for save method string
   var table;

   function fetchStocks() {
     return $.ajax({
       url : "<?php echo site_url('/api/stock/all_stocks/')?>",
       type: "GET",
       dataType: "JSON"
     }).then(function (data) {
       if (data.status) {
         $("#stock_table").empty();
         data.data.forEach((custObj)=>{
           var tr = $('<tr/>');
           tr.append("<td>" + custObj.StockId + "</td>");
           tr.append("<td>" + custObj.ProductCode + "</td>");
           tr.append("<td>" + custObj.SizeName + "</td>");
           tr.append("<td>" + custObj.Quantity + "</td>");
           tr.append('<td>' +
           '<button class="btn btn-warning" onclick="edit_stock('+custObj.StockId+')"><i class="glyphicon glyphicon-pencil"></i></button>'+
           '<button class="btn btn-danger" onclick="delete_stock('+custObj.StockId+')"><i class="glyphicon glyphicon-remove"></i></button>'+
           '</td>');
           $("#stock_table").append(tr);

         });
       }
       return Promise.resolve(data);
     });
   }
   function add_stock()
   {
     save_method = 'add';
     $('[name="StockId"]').val(undefined);

     $('#form_validation')[0].reset(); // reset form on modals
     $("#ProductId").val(undefined).selectpicker('refresh');
     $("#SizeId").empty().selectpicker('refresh');
     $("#ColorId").empty().selectpicker('refresh');
     $('#customModal').modal('show'); // show bootstrap modal
   }

   function edit_stock(id)
   {
     save_method = 'update';
     $('#form_validation')[0].reset(); // reset form on modals

     //Ajax Load data from ajax
     $.ajax({
       url : "<?php echo site_url('/api/stock/stock_details/')?>" + id,
       type: "GET",
       dataType: "JSON",
       success: function(data)
       {
           $('[name="StockId"]').val(data.data.StockId);
           $('[name="Quantity"]').val(data.data.Quantity);
           $('#ProductId').val(data.data.ProductId);
           $("#ProductId").selectpicker('refresh');
           fetchProductColor( data.data.ProductId , data.data.ColorId );
           fetchProductSize( data.data.ProductId , data.data.SizeId );
           $('#customModal').modal('show'); // show bootstrap modal when complete loaded
           $('.modal-title').text('Edit Stock'); // Set title to Bootstrap modal title

       },
       error: function (jqXHR, textStatus, errorThrown)
       {
           alert('Error get data from ajax');
       }
   });
   }



   function save()
   {
     var url;
     if(save_method == 'add')
     {
         url = "<?php echo site_url('/api/stock/save_stock')?>";
     }
     else
     {
       url = "<?php echo site_url('/api/stock/stock_update')?>";
     }

      // ajax adding data to database
         $.ajax({
           url : url,
           type: "POST",
           data: $('#form_validation').serialize(),
           dataType: "JSON",
           success: function(data)
           {
              //if success close modal and reload ajax table
              $('#customModal').modal('hide');
              fetchStocks();
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error adding / update data');
           }
       });
   }

   function delete_stock(id)
   {
     if(confirm('Are you sure delete this data?'))
     {
       // ajax delete data from database
         $.ajax({
           url : "<?php echo site_url('/api/stock/stock_update')?>/"+id,
           type: "POST",
           data :{
             StockId : id,
             Status : 1
           },
           dataType: "JSON",
           success: function(data)
           {
             fetchStocks();
              // location.reload();
           },
           error: function (jqXHR, textStatus, errorThrown)
           {
               alert('Error deleting data');
           }
       });

     }
   }
   function fetchProduct(){
     return $.ajax({
       url : "<?php echo site_url('/api/product/all_products')?>",
       type: "GET",
       dataType: "JSON"
     }).then(function (res) {
       if (res.status) {
         $("#ProductId").empty();
         var option = $('<option/>');
         // option.val(prodObj.ProductId );
         option.html("Select product");
         $("#ProductId").append(option);
         res.data.forEach(function (prodObj) {
             var option = $('<option/>');
             option.val(prodObj.ProductId );
             option.html( prodObj.ProductCode);
             $("#ProductId").append(option);
         });
         $("#ProductId").selectpicker('refresh');

       }
     })
   }

   function fetchProductColor(ProductId , ColorId) {
       return $.ajax({
         url : "<?php echo site_url('/api/productColor/product_colors')?>/"+ProductId,
         type: "GET",
         dataType: "JSON"
       }).then(function (res) {
           if (res.status) {
             $("#ColorId").empty();
             res.data.forEach(function (prodObj) {
                 var option = $('<option/>');
                 option.val(prodObj.ColorId );
                 option.html( prodObj.ColorName);
                 $("#ColorId").append(option);
             });
             if(ColorId){
               $("#ColorId").val(ColorId);
             }
             $("#ColorId").selectpicker('refresh');

           }
       })
   }

	function fetchProductSize(ProductId , SizeId) {
       return $.ajax({
         url : "<?php echo site_url('/api/productColor/product_sizes')?>/"+ProductId,
         type: "GET",
         dataType: "JSON"
       }).then(function (res) {
           if (res.status) {
             console.log($("#SizeId").empty());
             res.data.forEach(function (prodObj) {
                 var option = $('<option/>');
                 option.val(prodObj.SizeId );
                 option.html( prodObj.SizeName);
                 $("#SizeId").append(option);
             });
             if(SizeId){
               $("#SizeId").val(SizeId);//.prop('selected', true);
             }
             $("#SizeId").selectpicker('refresh');

           }
       })
   }

   </script>
