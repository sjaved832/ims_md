<style>
.open > .dropdown-menu {
    display: block;
    overflow: initial !important;
}
.bootstrap-select{
  padding: 2px !important;
}
</style>


<script type="text/javascript">
  <?php
  if(!isset($SaleId)){
    $SaleId = "null";
  }
  ?>

  function gotoSalesorder() {
    window.location.href = baseUrl +"salesorder";
  }
</script>
<section class="content">
    <div class="container-fluid">
      <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">
                  <div class="header">
                      <h2>SALES ORDER Returns</h2>
                      <ul class="header-dropdown m-r--5">
                          <li class="dropdown">
                              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                  <i class="material-icons">more_vert</i>
                              </a>
                              <ul class="dropdown-menu pull-right">
                                  <li><a href="javascript:void(0);" onclick="gotoSalesorder();">Cancel</a></li>
                                  <!-- <li><a href="javascript:void(0);">Another action</a></li>
                                  <li><a href="javascript:void(0);">Something else here</a></li> -->
                              </ul>
                          </li>
                      </ul>
                  </div>
                  <div class="body">
                    <div class="right_col" role="main">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                                     <div class="x_panel">

                      <div class="x_content">
                          <form class="form-horizontal form-label-left sales_form" method="post" novalidate>
                                           <div class="col-md-6" style="float:center;">
                                               <p>
                                                   <b>Customer Name</b>
                                               </p>
                                               <select id="CustomerId" name="CustomerId" class="form-control show-tick" data-live-search="true">
                                               </select>
                                           </div>
                      <div class="row-fluid" id='pro_details' style="">
                      <div class=" span12" style="height: auto; overflow: inherit;">
                      <div class="box-content span8" style="">

                             <input type="hidden"  name="hdn_amt" id="hdn_amt"/>
                             <div class="">
                               <table id="dataTable"  class="table table-striped table-bordered" align="center">
                                 <thead>
                                   <tr>
                                       <th class="" role="columnheader">Product</th>
                                       <th class="" role="columnheader">Color</th>
                                       <th class="" role="columnheader">Size</th>
                                       <th class="" role="columnheader">Qty.</th>
                                       <!-- <th class="" role="columnheader">Product price.</th>
                                       <th class="" role="columnheader">GST</th>
                                       <th class="" role="columnheader">Amount</th> -->
                                       <th class="" role="columnheader">Action</th>
                                   </tr>
                                 </thead>
                                 <tbody id="product_body">
                                 </tbody>
                              </table>
                            </div>
                              <span style="margin-right:20px;">
                                <button type="button" class="btn btn-primary" onClick="addRow()"><i class="glyphicon glyphicon-plus"></i></button>

                                <!-- <button type="button" value="Add" class name="btn_0" /> -->
                              </span>
                              <!-- <table width="100%">
                                  <tr>
                                    <td>Total Amount:</td>
                                    <td><input  style="width:100%;padding-left: 29px;" class="span8" type="text"
                                      name="txt_total" id="txt_total" onchange="grant_total()" readonly="readonly"/></td></td>
                                  </tr>
                                  <tr>
                                    <td>Discount (Rs)</td>
                                    <td><input style="width:100%;padding-left: 29px;" class="span12" type="text" name="discount"
                                        id="discount" maxlength="4" /></td>
                                   </tr>
                                   <tr>
                                     <td>Grand Total:</td>
                                     <td>
                                       <input style="width:100%;padding-left: 29px;" class="span8" type="text" id="TotalPay" name="TotalPay" readonly="readonly"/>
                                     </td>
                                   </tr>
                              </table> -->
                      </div>
                      </div>
                      </div>
                      <div class="row-fluid">
                         <div class="form-actions">
                          <input type="hidden" name="tax" id="tax" value="0.00">
                          <input type="hidden" name="vat" id="vat" value="0.00">
                          <input type="hidden" name="lbt" id="lbt" value="0.00">
                          <input type="hidden" name="row_count" id="row_count" value="2">
                         </div>
                      </div>
                             <div class="ln_solid"></div>
                             <div class="form-group">
                               <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                 <button type="button" class="btn btn-primary">Cancel</button>
                                 <button class="btn btn-primary" type="reset">Reset</button>
                                 <button id="sales_submit" type="submit" class="btn btn-success">Submit</button>
                               </div>
                             </div>
                      </form>
                    </div>
                    </div>
                    </div>
                      <div class="clearfix"></div>
                    </div>

                    </div>
                  </div>
              </div>
            </div>
      </div>

    </div>
</section>
<template id="productRow">
    <tr>
        <td>
          <input type="hidden" id="StockId" name="StockId[]" value=""/>

          <input type="hidden" id="Valid" name="Valid[]" value=""/>

           <select id="ProductId" class="form-control show-tick productSelect"  name="ProductId[]" data-live-search="true">
           </select>
        </td>
        <td>
            <select id="ColorId" class="form-control show-tick" name="ColorId[]" data-live-search="true">
            </select>
        </td>

        <td>
           <select id="SizeId" class="form-control show-tick" name="SizeId[]" data-live-search="true">
           </select>
        </td>
        <td><input style="width:100%;" class="span12" type="text" name="Quantity[]" required='required' id="Quantity" maxlength="4" /></td>
        <!-- <td><input  style="width:100%;" class="span12" type="text" name="Price[]" required='required' id="Price" readonly="readonly" /></td> -->
         <!-- td><input class="span12" type="text" name="txt_selprice_1" id="txt_selprice_1" readonly onkeypress="return isFloat(event);" onchange="calculate(1)"/></td -->
         <!-- <td>
            <select id="GST" class="form-control show-tick" name="GST[]" data-live-search="true">
                <option value="18">18%</option>
                <option value="12">12%</option>
                <option value="5">5%</option>
                <option value="0">0%</option>
            </select>
         </td>

        <td>
          <input  style="width:100%;" class="span12" type="text" name="total_price[]" id="TotalAmount" readonly="readonly" />
        </td> -->
        <td>
          <button class="btn btn-danger delete_row" ><i class="glyphicon glyphicon-remove"></i></button>
        </td>
    </tr>
</template>
<!-- JQuery Steps Plugin Js -->
<script src="<?=base_url('plugins/jquery-steps/jquery.steps.js')?>"></script>

<!-- Sweet Alert Plugin Js -->
<script src="<?=base_url('plugins/sweetalert/sweetalert.min.js')?>"></script>




<script type="text/javascript">
      var ProductId , SizeList , ProductList;
      $(document).ready(function(){
        fetchCustomers();
        // fetchProducts();
        addRow();
      });
      $(".sales_form").submit(function (e) {
        $("#sales_submit").attr("disabled", true);
        e.preventDefault();
        $.ajax({
          "url":baseUrl + "api/sale/return_sales_order",
          "type" : "POST",
          "data" : $(this).serialize(),
          "success":function (res) {
            $("#sales_submit").attr("disabled", false );
            // if (res.status) {
            //   gotoSalesorder();
            // }
          }
        });
      })
      $(document).on("change","#ProductId",function (event) {
        ProductId = this.value;
        fetch_product_size(this);
        fetch_product_color(this);
      });

      $(document).on("change","#SizeId",function (event) {
        var value = this.value;
        var index = SizeList.map(function(e) { return e.SizeId; }).indexOf(value);

        var element =  $(this).closest("tr").find("#Price");
        element.val(SizeList[index].ProductPrice);
        changePrice(this);
      });
      $(document).on("click",".delete_row",function () {
        if (confirm("Are you sure!")) {

          var trElm = $(this).closest("tr").empty();
        }
      })
      $(document).on("blur","#Quantity",function (event) {
        changePrice(this);
        // grandTotal();
      });
      // $(document).on("change","#ColorId",function () {
      //   changePrice(this);
      // })
      //   $(document).on("change","#GST",function (event) {
      //     changePrice(this,true);
      //     grandTotal();
      //   });
      //
      //    $(document).on("change","#discount",function (event) {
      //     changePrice(this);
      //     grandTotal();
      //   });



        /***
        * Generic Total Calculate
        **/
      function changePrice(event ,flag) {
        var mainTr = $(event).closest("tr");

        var Quantity = Number(mainTr.find("#Quantity").val());

        var productPrice = Number(mainTr.find("#Price").val());

        var GSTPrice = Number(mainTr.find("#GST").val());

        var ColorId = Number(mainTr.find("#ColorId").val());

        var SizeId = Number(mainTr.find("#SizeId").val());

        var ProductId = Number(mainTr.find("#ProductId").val());

        ColorId = isNaN(ColorId) ? 0 : ColorId;
        SizeId = isNaN(SizeId) ? 0 : SizeId;

        if (Quantity != 0  && ColorId != 0 &&  SizeId != 0  && !flag) {
          let obj = {
            ProductId,
            ColorId,
            SizeId,
            Quantity
          }
          $.ajax({
            url : baseUrl + "api/stock/available_stock",
            type : "get",
            data : obj,
            success: function (res) {
              // if (!res.status) {
              //   $(event).closest("tr").find("td").each(function () {
              //     $(this).css("border-color","red");
              //   })
              // }else if(Number(res.data.Quantity) < Quantity && Quantity > 0){
              //   $(event).css("border-color","red");
              // }else{
              //   $(event).closest("tr").find("td").each(function (){
              //     $(this).css("border-color","#eee");
              //   });
                $(event).closest("tr").find("#StockId").val(res.data.StockId);
                $(event).closest("tr").find("#Valid").val(true);
              //   return $(event).css("border-color","#eee");
              // }
              // $(event).closest("tr").find("#Valid").val(false);
            }
          })
        }

        var TotalPrice = ( productPrice * Quantity ) + ( ( productPrice * Quantity ) * ( GSTPrice / 100 ) );
        var TotalAmountElement = mainTr.find("#TotalAmount");
        TotalAmountElement.val(TotalPrice);
        return TotalPrice;
      }

      function grandTotal() {
        var total = 0;
        $("#product_body").find("tr").each(function (elem,tar) {
          total = total + changePrice(this ,true); //console.log();
        })
        $("#txt_total").val(total);

        var discount = 0;

        $("#discount").each(function() {
          discount = this.value;  //console.log();
                })
        $("#discount").val(discount);


        var result = 0;

        $("#product_body").each(function() {
          result = total + result - discount; //console.log();
        })
        $("#TotalPay").val(result);

      }

      function fetch_product_color( target ) {
        return $.ajax({
            url : baseUrl + "api/productColor/product_colors/" + ProductId,
            dataType : "JSON",
            type : "GET"
          }).then(function (res) {
            if (res.status && res.data) {
              var element =  $(target).closest("tr").find("#ColorId");
              element.empty();

              let option = $("<option/>");
              option.html("Please Select Color");
              element.append(option);
              res.data.forEach((prodObj)=>{
                if ( prodObj.Status == 0 ) {
                  let option = $("<option/>");
                  option.html(prodObj.ColorName);
                  option.val(prodObj.ProductColorId);
                  element.append(option);
                }
              });
              element.selectpicker('refresh');
            }
            return Promise.resolve(res);
          });
      }
      function fetch_product_size( target ) {
        return $.ajax({
          url : baseUrl + "api/productColor/product_sizes/" + ProductId,
          dataType : "JSON",
          type : "GET"
        }).then(function (res) {
          if (res.status && res.data) {
              var element =  $(target).closest("tr").find("#SizeId");
              element.empty();
              SizeList = res.data;
              let option = $("<option/>");
              option.html("Please Select Size");
              element.append(option);
              res.data.forEach((prodObj)=>{
                if( prodObj.Status == 0 ) {
                    let option = $("<option/>");
                    option.html(prodObj.SizeName);
                    option.val(prodObj.SizeId);
                    element.append(option);
                }
              });
              element.selectpicker('refresh');
          }
          return Promise.resolve(res);
        });
      }


      function fetchCustomers() {
            return $.ajax({
              url : "<?php echo site_url('/api/customer/all_customers/')?>",
              type: "GET",
              dataType: "JSON"
            }).then(function (data) {
              if (data.status) {
                $("#CustomerId").empty();
                data.data.forEach((custObj)=>{
                  var option = $('<option/>');
                  option.html( custObj.CustomerName );
                  option.val(custObj.CustomerId );
                  $("#CustomerId").append(option);

                });
                $("#CustomerId").selectpicker('refresh');

              }
              return Promise.resolve(data);
            });
        }

       function fetchProducts() {
         var element = $("#product_body tr:last").find("#ProductId").empty();

         let option = $('<option/>');
         option.html("Please select product");
         element.append(option);

         if(!ProductList){
             return $.ajax({
               url : "<?php echo site_url('/api/product/all_products/')?>",
               type: "GET",
               dataType: "JSON"
             }).then(function (data) {
               if (data.status) {
                 ProductList = data.data;
                 data.data.forEach((custObj)=>{
                     var option = $('<option/>');
                     option.html( custObj.ProductCode );
                     option.val(custObj.ProductId );
                     element.append(option);
                });
                 element.selectpicker('refresh');
               }
               return Promise.resolve(data);
             });
          }else {
              // var element = $("tbody tr:last");//.find("#ProductId").empty();
              ProductList.forEach((custObj)=>{
                  var option = $('<option/>');
                  option.html( custObj.ProductCode );
                  option.val(custObj.ProductId );
                  element.append(option);
             });
              element.selectpicker('refresh');
          }
       }


      function addRow(tableID) {
        // Clone the new row and insert it into the table
        var t = document.querySelector('#productRow');

        var tb = document.querySelector("tbody");

        var clone = document.importNode(t.content, true);
        tb.appendChild(clone);
        fetchProducts()
      }

      function frm_save()
      {
             return true;
      }


</SCRIPT>
