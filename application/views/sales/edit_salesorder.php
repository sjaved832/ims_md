<style>
.open > .dropdown-menu {
    display: block;
    overflow: initial !important;
}
.bootstrap-select{
  padding: 2px !important;
}
</style>


<script type="text/javascript">
  <?php
  if(!isset($SalesOrderId)){
    $SalesOrderId = null;
  }
  ?> var SalesOrderId = <?=$SalesOrderId?>

  function gotoSalesorder() {
    window.location.href = baseUrl +"salesorder";
  }
</script>
<section class="content">
    <div class="container-fluid">
      <div class="row clearfix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="card">
                  <div class="header">
                      <h2>SALES ORDER</h2>
                      <ul class="header-dropdown m-r--5">
                          <li class="dropdown">
                              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                  <i class="material-icons">more_vert</i>
                              </a>
                              <ul class="dropdown-menu pull-right">
                                  <li><a href="javascript:void(0);" onclick="gotoSalesorder();">Cancel</a></li>
                              </ul>
                          </li>
                      </ul>
                  </div>
                  <div class="body">
                    <div class="right_col" role="main">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="x_panel">
                        <div class="x_content">
                            <form class="form-horizontal form-label-left sales_form" method="post" novalidate>
                               <div class="col-md-6" style="float:center;">
                                   <p>
                                       <b>Customer Name : </b>
                                       <span for="" id="CustomerName"></span>
                                   </p>
                               </div>
                      <div class="row-fluid" id='pro_details' style="">
                      <div class=" span12" style="height: auto; overflow: inherit;">
                      <div class="box-content span8" style="">
                             <div class="">
                               <table id="dataTable"  class="table table-striped table-bordered" align="center">
                                 <thead>
                                   <tr>
                                      <th></th>
                                       <th class="" role="columnheader">Product</th>
                                       <th class="" role="columnheader">Color</th>
                                       <th class="" role="columnheader">Size</th>
                                       <th class="" role="columnheader">Qty.</th>
                                       <th class="" role="columnheader">Product price.</th>
                                       <th class="" role="columnheader">GST</th>
                                       <th class="" role="columnheader">Amount</th>
                                   </tr>
                                 </thead>
                                 <tbody id="product_body">
                                 </tbody>
                              </table>
                              <div class="form-group">
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                  <button type="button" id="addReturns" disabled="disabled" onclick="add_sales_return()" class="btn btn-primary">Add Returns</button>
                                </div>
                              </div>
                            </div>
                              <table width="50%">
                                  <tr>
                                    <td>Total Amount:</td>
                                    <td><span class="span8" id="txt_total"></span></td></td>
                                  </tr>
                                  <tr>
                                    <td>Discount (Rs)</td>
                                    <td><span class="span12" id="discount" ></span></td>
                                   </tr>
                                   <tr>
                                     <td>Grand Total:</td>
                                     <td>
                                       <span class="span8" id="TotalPay"></span>
                                     </td>
                                   </tr>
                              </table>
                      </div>
                      </div>
                      </div>

                       <div class="ln_solid"></div>
                       <div class="form-group">
                         <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                           <button type="button" class="btn btn-primary">Cancel</button>
                           <button id="sales_submit" type="submit" class="btn btn-success">Submit</button>
                         </div>
                       </div>
                      </form>
                    </div>
                    </div>
                    </div>
                      <div class="clearfix"></div>
                    </div>

                    </div>
                  </div>
              </div>
            </div>
      </div>

    </div>
    <div class="modal fade" id="customModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Returns</h4>
                </div>
                <form id="returnForm" action="#">
                    <div class="modal-body">
                      <div class="row clearfix">
                        <div class="col-sm-12">
                        <input type="hidden" id="SalesOrderId" name="SalesOrderId"/>
                         <table id="dataTable1"  class="table table-striped table-bordered" align="center">
                             <thead>
                               <tr>
                                   <th class="" role="columnheader">Product</th>
                                   <th class="" role="columnheader">Color</th>
                                   <th class="" role="columnheader">Size</th>
                                   <th class="" role="columnheader">Qty.</th>
                                   <th class="" role="columnheader">Price.</th>
                                   <th class="" role="columnheader">return Qty.</th>
                               </tr>
                             </thead>
                             <tbody id="product_body1">
                             </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" disabled="disabled" id="returnSubmit" class="btn btn-link waves-effect" >SAVE </button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
              </form>

            </div>
        </div>
    </div>
</section>
<template id="productRow">
    <tr>
      <td>
        <input type="checkbox" onchange="changeChecked(this)" id="returnChecked"/>
      </td>
        <td>
          <span class="ProductId"></span>
        </td>
        <td>
          <span class="ColorId"></span>
        </td>
        <td>
          <span class="SizeId"></span>
        </td>
        <td>
          <span class="Quantity"></span>
        </td>
        <td>
          <span class="Price"></span>
        </td>
         <td>
           <span class="GST"></span>
         </td>
        <td>
          <span class="TotalAmount"></span>
        </td>
    </tr>
</template>
<!-- JQuery Steps Plugin Js -->
<script src="<?=base_url('plugins/jquery-steps/jquery.steps.js')?>"></script>

<!-- Sweet Alert Plugin Js -->
<script src="<?=base_url('plugins/sweetalert/sweetalert.min.js')?>"></script>




<script type="text/javascript">
      var ProductList;
      $(document).ready(function(){
        fetchSalesOrder();
      });

      function fetchSalesOrder(){

        $.ajax({
          "url":baseUrl + "api/sale/sales_details/"+SalesOrderId,
          "type" : "GET",
          "data" : $(this).serialize(),
          "success": function (res) {
                      if(!res.status){
                        return gotoSalesorder();
                      }
                      render_sales_page(res.data);
                      ProductList = res.data.ProductList;
                     }
          });
      };
      function changeChecked(env){
        var elem =$(env);
        var index = Number(elem.attr("index") || 0 );
        ProductList[index].checked = elem.prop("checked");
        if( ProductList.some( prodObj => prodObj.checked )){
          $("#addReturns").attr("disabled",false);
        }else {
          $("#returnSubmit").attr("disabled" , true);
          $("#addReturns").attr("disabled" , true);
        }
      }

      function add_sales_return(){
        $('#customModal').modal('show');
        $("#SalesOrderId").val(SalesOrderId);
        var elem = $("#product_body1").empty();
        ProductList
            .filter(prodObj => prodObj.checked)
                .forEach(function (prodObj) {
                        var tr = $('<tr/>');
                        tr.append("<td><input value='"+prodObj.StockId+"' type='hidden' name='StockId[]' /> <input type='hidden' value='"+prodObj.SalesOrderEntryId+"' name='SalesOrderEntryId[]'/>" + prodObj.ProductCode + "</td>");
                        tr.append("<td><input type='hidden' name='GST[]' value="+prodObj.GST+" /><input type='hidden' name='ProductPrice[]' class='ProductPrice'/><input type='hidden' name='Valid[]' class='Valid'/>" + prodObj.SizeName + "</td>");
                        tr.append("<td>" + prodObj.ColorName + "</td>");
                        tr.append("<td>" + prodObj.Quantity + "</td>");
                        tr.append("<td class='Product' data='"+JSON.stringify(prodObj) +"'>0</td>");
                        tr.append("<td id='actualQuantity'><input type='number' onblur='checkQuantity(this)' name='Quantity[]' data='" + prodObj.Quantity +"' /></td>");
                        elem.append(tr);
                })
      };

      function checkQuantity(env) {
        var elem = $(env);
        var rootTr = $(env).closest("tr");
        var ValidElem = rootTr.find(".Valid");
        var ProductPriceElem = rootTr.find(".ProductPrice");
        var ProductElem = rootTr.find(".Product");
        var Product= JSON.parse(ProductElem.attr("data"));
        var actualQuantity = Number(elem.attr("data"));
        var Quantity = Number(elem.val());
        if (Quantity <= actualQuantity && Quantity > 0) {
          debugger;
          var ProductPrice = Quantity * Number(Product.ProductPrice);
          var TotalPrice = ProductPrice + (Number(Product.GST)/100 * ProductPrice );
          ProductElem.html(TotalPrice);
          ProductPriceElem.val(TotalPrice);
          ValidElem.val(true);
          $("#returnSubmit").attr("disabled",false);
          elem.css("border-color","initial");
          return true;
        }
        $("#returnSubmit").attr("disabled",true);
        ValidElem.val(false);
        elem.css("border-color","red");
      }
      function addRow(prodObj , index) {
        var clone = $($("#productRow").clone(true).html());
        fetch_product_by_id(prodObj.ProductId,clone,prodObj);
        fetch_product_size_by_id(prodObj.SizeId,clone,prodObj);
        fetch_product_color_by_id(prodObj.ColorId,clone,prodObj);
        clone.find("#returnChecked").attr("data",prodObj.SalesOrderEntryId)
        clone.find("#returnChecked").attr("index",index)
        clone.find(".Quantity").html(prodObj.Quantity);
        clone.find(".TotalAmount").html(prodObj.Pay);
        clone.find(".GST").html(prodObj.GST);
        $("#product_body").append(clone);
      }

      function render_sales_page(salesObj) {
        $("#CustomerName").html(salesObj.CustomerName);
        $("#txt_total").html(salesObj.TotalPay);
        $("#discount").html(salesObj.Discount);
        $("#TotalPay").html(salesObj.TotalPay);
        $("#product_body").empty();
        salesObj.ProductList.forEach(function (productObj,i) {
          addRow(productObj,i);
        })
      }


      function fetch_product_color_by_id(id , target,prodObj) {
        return $.ajax({
            url : baseUrl + "api/color/color_details/" + id,
            dataType : "JSON",
            type : "GET"
          }).then(function (res) {
            if (res.status) {
              prodObj.ColorName = res.data.ColorName;
              $(target).find(".ColorId").html(res.data.ColorName);
            }
          })
      }

      function fetch_product_size_by_id(id , target,prodObj) {
        return $.ajax({
            url : baseUrl + "api/productColor/size_details/" + id,
            dataType : "JSON",
            type : "GET"
          }).then(function (res) {
            if (res.status) {
            prodObj.ProductPrice = res.data.ProductPrice;
            prodObj.SizeName = res.data.SizeName;
            target.find(".Price").html(res.data.ProductPrice);
            target.find(".SizeId").html(res.data.SizeName);
            }
          });
      }

      function fetch_product_by_id(id , target ,prodObj) {
        return $.ajax({
            url : baseUrl + "api/product/product_details/" + id,
            dataType : "JSON",
            type : "GET"
          }).then(function (res) {
            if (res.status) {
              prodObj.ProductCode = res.data.ProductCode;
              target.find(".ProductId").html(res.data.ProductCode);
            }
          });
      }

      $("#returnForm").submit(function (e) {
        e.preventDefault();
        $("#returnSubmit").attr("disabled", false );
        $.ajax({
          "url":baseUrl + "api/sale/return_sales_order",
          "type" : "POST",
          "data" : $(this).serialize(),
          "success":function (res) {
            $("#returnSubmit").attr("disabled", false );
            if (res.status) {
              $('#customModal').modal('hide');
              fetchSalesOrder();
            }
          }
        });

      })
      ///**************************** OLD coding
      $(".sales_form").submit(function (e) {
        $("#sales_submit").attr("disabled", true);
        e.preventDefault();
        $.ajax({
          "url":baseUrl + "api/sale/save_sales_order",
          "type" : "POST",
          "data" : $(this).serialize(),
          "success":function (res) {
            $("#sales_submit").attr("disabled", false );
            if (res.status) {
              gotoSalesorder();
            }
          }
        });
      })
      $(document).on("change","#ProductId",function (event) {
        ProductId = this.value;
        fetch_product_size(this);
        fetch_product_color(this);
      });

      $(document).on("change","#SizeId",function (event) {
        var value = this.value;
        var index = SizeList.map(function(e) { return e.SizeId; }).indexOf(value);

        var element =  $(this).closest("tr").find("#Price");
        element.val(SizeList[index].ProductPrice);
        changePrice(this);
      });
      $(document).on("click",".delete_row",function () {
        if (confirm("Are you sure!")) {

          var trElm = $(this).closest("tr").empty();
        }
      })
      $(document).on("blur","#Quantity",function (event) {
        changePrice(this);
        grandTotal();
      });
      $(document).on("change","#ColorId",function () {
        changePrice(this);
      })
        $(document).on("change","#GST",function (event) {
          changePrice(this,true);
          grandTotal();
        });

         $(document).on("change","#discount",function (event) {
          changePrice(this);
          grandTotal();
        });



        /***
        * Generic Total Calculate
        **/
      function changePrice(event ,flag) {
        var mainTr = $(event).closest("tr");

        var Quantity = Number(mainTr.find("#Quantity").val());

        var productPrice = Number(mainTr.find("#Price").val());

        var GSTPrice = Number(mainTr.find("#GST").val());

        var ColorId = Number(mainTr.find("#ColorId").val());

        var SizeId = Number(mainTr.find("#SizeId").val());

        var ProductId = Number(mainTr.find("#ProductId").val());

        ColorId = isNaN(ColorId) ? 0 : ColorId;
        SizeId = isNaN(SizeId) ? 0 : SizeId;

        if (Quantity != 0  && ColorId != 0 &&  SizeId != 0  && !flag) {
          let obj = {
            ProductId,
            ColorId,
            SizeId,
            Quantity
          }
          $.ajax({
            url : baseUrl + "api/stock/available_stock",
            type : "get",
            data : obj,
            success: function (res) {
              if (!res.status) {
                $(event).closest("tr").find("td").each(function () {
                  $(this).css("border-color","red");
                })
              }else if(Number(res.data.Quantity) < Quantity && Quantity > 0){
                $(event).css("border-color","red");
              }else{
                $(event).closest("tr").find("td").each(function () {
                  $(this).css("border-color","#eee");
                })
                $(event).closest("tr").find("#StockId").val(res.data.StockId);
                $(event).closest("tr").find("#Valid").val(true);
                return $(event).css("border-color","#eee");
              }
              $(event).closest("tr").find("#Valid").val(false);
            }
          })
        }

        var TotalPrice = ( productPrice * Quantity ) + ( ( productPrice * Quantity ) * ( GSTPrice / 100 ) );
        var TotalAmountElement = mainTr.find("#TotalAmount");
        TotalAmountElement.val(TotalPrice);
        return TotalPrice;
      }

      function grandTotal() {
        var total = 0;
        $("#product_body").find("tr").each(function (elem,tar) {
          total = total + changePrice(this ,true);
        })
        $("#txt_total").val(total);

        var discount = 0;

        $("#discount").each(function() {
          discount = this.value;
                })
        $("#discount").val(discount);


        var result = 0;

        $("#product_body").each(function() {
          result = total + result - discount;
        })
        $("#TotalPay").val(result);

      }

      function fetch_product_color( target ) {
        return $.ajax({
            url : baseUrl + "api/productColor/product_colors/" + ProductId,
            dataType : "JSON",
            type : "GET"
          }).then(function (res) {
            if (res.status && res.data) {
              var element =  $(target).closest("tr").find("#ColorId");
              element.empty();

              let option = $("<option/>");
              option.html("Please Select Color");
              element.append(option);
              res.data.forEach((prodObj)=>{
                if ( prodObj.Status == 0 ) {
                  let option = $("<option/>");
                  option.html(prodObj.ColorName);
                  option.val(prodObj.ProductColorId);
                  element.append(option);
                }
              });
              element.selectpicker('refresh');
            }
            return Promise.resolve(res);
          });
      }
      function fetch_product_size( target ) {
        return $.ajax({
          url : baseUrl + "api/productColor/product_sizes/" + ProductId,
          dataType : "JSON",
          type : "GET"
        }).then(function (res) {
          if (res.status && res.data) {
              var element =  $(target).closest("tr").find("#SizeId");
              element.empty();
              SizeList = res.data;
              let option = $("<option/>");
              option.html("Please Select Size");
              element.append(option);
              res.data.forEach((prodObj)=>{
                if( prodObj.Status == 0 ) {
                    let option = $("<option/>");
                    option.html(prodObj.SizeName);
                    option.val(prodObj.SizeId);
                    element.append(option);
                }
              });
              element.selectpicker('refresh');
          }
          return Promise.resolve(res);
        });
      }

       function fetchProducts() {
         var element = $("#product_body tr:last").find("#ProductId").empty();

         let option = $('<option/>');
         option.html("Please select product");
         element.append(option);

         if(!ProductList){
             return $.ajax({
               url : "<?php echo site_url('/api/product/all_products/')?>",
               type: "GET",
               dataType: "JSON"
             }).then(function (data) {
               if (data.status) {
                 ProductList = data.data;
                 data.data.forEach((custObj)=>{
                     var option = $('<option/>');
                     option.html( custObj.ProductCode );
                     option.val(custObj.ProductId );
                     element.append(option);
                });
                 element.selectpicker('refresh');
               }
               return Promise.resolve(data);
             });
          }else {
              // var element = $("tbody tr:last");//.find("#ProductId").empty();
              ProductList.forEach((custObj)=>{
                  var option = $('<option/>');
                  option.html( custObj.ProductCode );
                  option.val(custObj.ProductId );
                  element.append(option);
             });
              element.selectpicker('refresh');
          }
       }

      function frm_save()
      {
             return true;
      }


</SCRIPT>
