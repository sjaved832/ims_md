var SizeList,priceList;
$(function () {
  var form = $('#product_form').show();
  form.steps({
      headerTag: 'h3',
      bodyTag: 'fieldset',
      transitionEffect: 'slideLeft',
      onInit: function (event, currentIndex) {
          $.AdminBSB.input.activate();

          //Set tab width
          var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
          var tabCount = $tab.length;
          $tab.css('width', (100 / tabCount) + '%');

          //set button waves effect
          setButtonWavesEffect(event);
      },
      onStepChanging: function (event, currentIndex, newIndex) {
          if (currentIndex > newIndex) { return true; }
          if (currentIndex < newIndex) {
              form.find('.body:eq(' + newIndex + ') label.error').remove();
              form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
          }

          form.validate().settings.ignore = ':disabled,:hidden';
          if(form.valid()){
            if(newIndex == 1){
              var productObj = {};
              productObj.ProductCode = $("#ProductCode").val();
              productObj.ProductPrice = $('#ProductPrice').val();
              productObj.BrandId = $("#BrandId").val();
              productObj.ProductDescription = $("#ProductDescription").val();

              saveProduct(productObj).then(function (data) {
                $("#ProductId").val(data.data);
                ProductId = data.data;
  			        fetchSize();
              },function (err) {
                alert("erro")
                swal("Somthing bad!", "Not Submitted!", "error");
              })
            }
            if (newIndex == 2) {
              fetchColor();
            }

          }
          return form.valid();
      },
      onStepChanged: function (event, currentIndex, priorIndex) {
          setButtonWavesEffect(event);
      },
      onFinishing: function (event, currentIndex) {
          form.validate().settings.ignore = ':disabled';
          return form.valid();
      },
      onFinished: function (event, currentIndex) {
          swal("Good job!", "Submitted!", "success");
      }
  });

  form.validate({
      highlight: function (input) {
          $(input).parents('.form-line').addClass('error');
      },
      unhighlight: function (input) {
          $(input).parents('.form-line').removeClass('error');
      },
      errorPlacement: function (error, element) {
          $(element).parents('.form-group').append(error);
      },
      rules: {
      }
  });

  $(document).on("change",".ProductColor" ,saveProductColor);
  $(document).on("change",".ProductSize" ,saveProductSize);
  $(document).on("blur","#ProductPrice",saveProductSizeByBlur)
  function setButtonWavesEffect(event) {
      $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
      $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
  }
    //Custom Validations ===============================================================================
    //Date
    $.validator.addMethod('customdate', function (value, element) {
        return value.match(/^\d\d\d\d?-\d\d?-\d\d$/);
    },
        'Please enter a date in the format YYYY-MM-DD.'
    );

    //Credit card
    $.validator.addMethod('creditcard', function (value, element) {
        return value.match(/^\d\d\d\d?-\d\d\d\d?-\d\d\d\d?-\d\d\d\d$/);
    },
        'Please enter a credit card in the format XXXX-XXXX-XXXX-XXXX.'
    );
    //==================================================================================================
});

  function saveProduct(formData) {
      if(!formData || typeof formData !== "object")
        return Promise.reject({ err : "Data missing" });
        let url = "api/product/save_product";
        if(ProductId){
          formData.ProductId = ProductId;
          url = "api/product/product_update";
        }

      return $.ajax({
          url:baseUrl+url,
          dataType: "JSON",
          data : formData,
          type:"POST"
        });
  }

  function saveProductColor(that) {
      if(!that)
        return Promise.reject({ err : "Data missing" });
        let url = "api/productColor/save_product_color";
        let formData = {
          ProductId : ProductId,
          ColorId : $(that.target).val(),
          Status : that.target.checked ? 0 : 1
        }

      return $.ajax({
          url:baseUrl+url,
          dataType: "JSON",
          data : formData,
          type:"POST"
        });
  }
  function saveProductSizeByBlur() {
    var that = this;
    var sizeData = $(that).attr("data");
    var ProductPrice = Number($(this).val());
    var index = SizeList.indexOf(sizeData);
    if(!priceList ){
      priceList = SizeList.map(size=>{ return {size}; });
    }
      priceList = priceList.map(function (tempObj,i) {
                      if(i == index){
                        tempObj.ProductPrice = ProductPrice;
                      }else if(i > index && !tempObj.ProductPrice){
                        tempObj.ProductPrice = ((i - index) * 50 ) + ProductPrice;
                      }else if(!tempObj.ProductPrice){
                        tempObj.ProductPrice = ProductPrice - ((index - i) * 50 );
                      }
                      if(tempObj.ProductPrice < 0 && !tempObj.ProductPrice){
                        tempObj.ProductPrice = 0;
                      }
                      return tempObj
                    })
    priceList.forEach(function (priceObj) {
      $(".ProductPrice"+priceObj.size).val(priceObj.ProductPrice);
    });
    var ProductSize = $(this).closest("tr").find(".ProductSize");
    let url = "api/productColor/save_product_size";
    let formData = {
      ProductId : ProductId,
      SizeName : $(ProductSize["0"]).attr("data"),
      Status : ProductSize["0"].checked ? 0 : 1,
      ProductPrice : ProductPrice
    };

    return $.ajax({
        url:baseUrl+url,
        dataType: "JSON",
        data : formData,
        type:"POST"
      });
  }
   function saveProductSize(that) {
     var ProductPrice = $(that.target).closest("tr").find("#ProductPrice");
     // Need to Add Validation
     if( ProductPrice["0"] && ($(ProductPrice["0"]).val() == "" ) || $(ProductPrice["0"]).val() ){
       $(ProductPrice["0"]).attr("required","required");
       $(ProductPrice["0"]).rules("add",{required:true});
     }

      if(!that && $("#product_form").valid())
        return Promise.reject({ err : "Data missing" });
        let url = "api/productColor/save_product_size";
        let formData = {
          ProductId : ProductId,
          SizeName : $(that.target).val(),
          Status : that.target.checked ? 0 : 1,
          ProductPrice : $(ProductPrice["0"]).val()
        }

      return $.ajax({
          url:baseUrl+url,
          dataType: "JSON",
          data : formData,
          type:"POST"
        });
  }

function fetch_product_color() {
  return $.ajax({
      url : baseUrl + "api/productColor/product_colors/" + ProductId,
      dataType : "JSON",
      type : "GET"
    }).then(function (res) {
      if (res.status && res.data) {
        res.data.forEach((prodObj)=>{
          if ( prodObj.Status == 0 ) {
            $("#ProductColor" + prodObj.ColorId).prop('checked', true);//.attr("checked","checked");
          }
        });
      }
      return Promise.resolve(res);
    });
}
	function fetch_product_size() {
	  return $.ajax({
		  url : baseUrl + "api/productColor/product_sizes/" + ProductId,
		  dataType : "JSON",
		  type : "GET"
		}).then(function (res) {
		  if (res.status && res.data) {
			res.data.forEach((prodObj)=>{
			  if( prodObj.Status == 0 ) {
				      $("#ProductSize" + prodObj.SizeName).prop('checked', true);//.attr("checked","checked");
			  }
        var ProductPriceObj = $(".ProductPrice" + prodObj.SizeName);
        ProductPriceObj.val(prodObj.ProductPrice);
        ProductPriceObj.trigger("blur");
			});
		  }
		  return Promise.resolve(res);
		});
	}

  function fetchBrands() {
    return $.ajax({
      url : baseUrl+"api/brand/all_brands",
      type: "GET",
      dataType: "JSON"
    }).then(function (data) {
      if (data.status) {
        $("#BrandId").empty();
        data.data.forEach((custObj)=>{
          var option = $('<option/>');
          option.val(custObj.BrandId );
          option.html( custObj.BrandName);
          $("#BrandId").append(option);
        });
        $("#BrandId").selectpicker('refresh');
      }
      return Promise.resolve(data);
    });
  }

   function fetchSize() {
        return $.ajax({
          url : baseUrl + "/api/color/all_sizes/",
          type: "GET",
          dataType: "JSON"
        }).then(function ( res ) {
          if (res.status) {
            $("#SizeList").empty();
            SizeList = res.data;
            res.data.forEach((size)=>{
              var tr = $("<tr/>");
              var td1 = $("<td/>");
              var div1 = $('<div/>');
              div1.attr("class","form-group");
              var input = $("<input type='checkbox' class='ProductSize' data='"+size+"' id='ProductSize"+size+"' name='ProductSize"+size+"' value='"+ size +"'>");
              var label = $("<label/>");
              label.attr("for","ProductSize"+size);
              label.html(size);
              div1.append(input);
              div1.append(label);
              td1.append(div1);

              var div2 = $('<div/>');
              var td2 = $("<td/>");
              var div3 = $('<div/>');

              div2.attr("class","form-group");
              div3.attr("class","form-line");

              var input1 = $("<input type='number' aria-required='true' data='"+size+"' class='form-control ProductPrice"+size+"' id='ProductPrice' name='ProductPrice"+size+"' >");
              var label1 = $("<label/>").html("Product Price").attr("class","form-label").css({"top":"-13px"});

              div3.append(input1);
              div3.append(label1);
              div2.append(div3);
              td2.append(div2);

              tr.append(td1);
              tr.append(td2);
              $("#SizeList").append(tr);

            });
          }

          fetch_product_size();
          return Promise.resolve(res);
        });
      }

      function fetchColor() {
        return $.ajax({
          url : baseUrl + "/api/color/all_colors/",
          type: "GET",
          dataType: "JSON"
        }).then(function (data) {
          if (data.status) {
            $("#ColorList").empty();
            data.data.forEach((custObj)=>{
              var div = $('<div/>');
              div.attr("class","form-group");
              var input = $("<input type='checkbox' class='ProductColor' id='ProductColor"+custObj.ColorId+"' name='ProductColor[]' value='"+ custObj.ColorId +"'>");
              var label = $("<label/>");
              label.attr("for","ProductColor"+custObj.ColorId);
              label.html(custObj.ColorName);
              div.append(input);
              div.append(label);

              $("#ColorList").append(div);

            });
          }

          fetch_product_color();

          return Promise.resolve(data);
        });
      }
      function loadProduct( ProductId ) {
        return $.ajax({
          url : baseUrl + "/api/product/product_details/" + ProductId,
          type: "GET",
          dataType: "JSON"
        }).then(function (data) {
          if( data.status ){
            $("#ProductCode").val( data.data.ProductCode ).trigger("click");
            $('#ProductPrice').val( data.data.ProductPrice );
            $("#BrandId").val( data.data.BrandId ).selectpicker('refresh');
            $("#ProductDescription").val( data.data.ProductDescription );
          }
          return Promise.resolve(data);
        });
      }
